
package lapr1;



import java.util.Formatter;
import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;

//Classe com métodos que envolvem cálculos necessários para implementar o método AHP (vetores e valores próprios, normalização de matrizes, etc.)

public class AHP {
    //Variáveis globais da classe
    public static final double[] TABELA_IR = {0.00,0.00,0.58,0.90,1.12,1.24,1.32,1.41,1.45,1.49,1.51,1.48,1.56,1.57,1.59,};
    
    
    /**
     * Método que transforma um array bidimensional num objeto Matrix da biblioteca l4aj
     * @param array, array bidimensional dado
     * @return objeto Matrix da biblioteca l4aj
     */
    
    public static Matrix array_Matrix(double[][] array){
        Matrix matriz = Matrix.from2DArray(array);
        return matriz;
    }
    
    /**
     * Método que calcula o vetor próprio exato de uma dada matriz usando a biblioteca la4j
     * @param array, array bidimensional dado
     * @return vetor próprio exato associado à matriz dada
     */
    
    public static double[] EigenVectorExato(double[][] array){
       double[] eigenvector = new double[array.length];
       Matrix a = new Basic2DMatrix(array);
       EigenDecompositor eigenD=new EigenDecompositor(a);
       Matrix [] eigenMatrix= eigenD.decompose();
       Matrix m = eigenMatrix[0].toDenseMatrix();
       double[][] temp = m.toDenseMatrix().toArray();
       Matrix m2 = eigenMatrix[1].toDenseMatrix();
       double [][] temp2 = m2.toDenseMatrix().toArray();
       double val = m2.max();
       int pos=0;
       for(int i=0;i<temp2.length;i++){
           for(int j=0;j<temp2.length;j++){
               if(temp2[i][j]==val){
                    pos=j;
                    break;
               }
           }
       }
       
       for(int i=0;i<eigenvector.length;i++){
           if(temp[i][pos]<0){
                eigenvector[i]=temp[i][pos]*(-1);
           }else{
               eigenvector[i]=temp[i][pos];
           }
       }
       
    return normalizar_Vetor(eigenvector);
    }
    
    /**
     * Método que calcula o vetor próprio aproximado de uma matriz dada
     * @param array, array bidimensional dado
     * @return vetor próprio aproximado da matriz dada
     */
    
    public static double[] EigenVectorAprox(double[][] array){
       double[] eigenvector = new double[array.length];
       double soma=0;
       for(int i=0;i<array.length;i++){
           for(int k=0;k<array[0].length;k++){
               soma=soma+(array[i][k]);
           }
           double media=soma/(array.length);
           eigenvector[i]=media;
           soma=0;
       }
       return eigenvector;
    }
    
    /**
     * Método que calcula o valor próprio máximo exato de uma matriz dada usando a biblioteca la4j
     * @param array, array bidimensional dado
     * @return valor próprio máximo exato da matriz dada
     */
    
    public static double maxEigenValueExato(double[][] array){
        double max_eigenvalue;
        Matrix matriz = array_Matrix(array);
        EigenDecompositor eigenD = new EigenDecompositor(matriz);
        Matrix[] eigenmatrix = eigenD.decompose();
        Matrix m =eigenmatrix[1].toDenseMatrix();
        max_eigenvalue = m.max();
        return max_eigenvalue;
    }
    
    /**
     * Método que calcula o valor próprio aproximado de uma matriz dada
     * @param array, array bidimensional dado
     * @param vector, vetor próprio aproximado associado ao array 
     * @return valor próprio máximo aproximado da matriz dada
     */
    
     public static double maxEigenValueAprox(double[][] array,double[] vector){
        double[] array2=new double[vector.length];
        double soma=0,soma2=0,media=0;
        for(int i=0;i<array.length;i++){
            for(int k=0;k<array[0].length;k++){
               soma=soma+((array[i][k])*(vector[k])); 
            }
            array2[i]=soma;
            soma=0;
        }
        for(int j=0;j<array.length;j++){
            soma2=soma2+((array2[j])/(vector[j]));
        }
        media=soma2/(array.length);
        return media;
    }
     
     /**
      * Método que normaliza uma matriz dada
      * @param matriz, array bidimensional dado
      */
     
    public static void normalizarMatriz(double [][] matriz){
        
        double [] array= new double [matriz[0].length];
        double soma=0;
                
        for(int j=0;j<matriz.length;j++){
            
            for(int i=0;i<matriz[0].length;i++){
                array[i]=matriz[i][j];
            }
            
            for(int l=0;l<array.length;l++){
                soma=soma+array[l];
            }
            
            for(int k=0;k<array.length;k++){
                array[k]=array[k]/soma;
                matriz[k][j]=array[k];
            }
            soma=0;
        }
    }
    
    /**
     * Método que calcula a Razão de Consistência de uma dada matriz
     * @param ic, Índice de Consistência da matriz
     * @param matriz, array bidimensional dado
     * @param n_crit, nº de critérios
     * @param n_alt, nº de alternativas
     * @return Razão de Consistência da matriz dada
     */
    
    public static double calcularRC(double ic,double[][] matriz,int n_crit, int n_alt){
        
        double rc=0;
        
        if(matriz.length==n_crit){
            rc=ic/TABELA_IR[n_crit-1];
        }else if(matriz.length==n_alt){
            rc=ic/TABELA_IR[n_alt-1];
        }
        return rc; 
    }
    
    /**
     * Método que calcula o Índice de Consistência de uma matriz dada
     * @param vpmax, valor próprio máximo da matriz
     * @param matriz, array bidimensional dado
     * @return Índice de Consistência da matriz dada
     */
    
    public static double calcularIC(double vpmax,double[][]matriz){
        double ic=(vpmax-(matriz.length))/((matriz.length)-1);
        return ic;
    }
    
    /**
     * Método que cria uma matriz (Matriz Prioridade) através da junção de 3 vetores
     * @param array
     * @return array bidimensional MatrizPrioridade
     */
    
    public static double [][] matrizPrioridade(double[][] array){
        double[][] MatrizPrioridade = new double[array[0].length][array.length];
        for(int i=0;i<MatrizPrioridade.length;i++){    
            for(int j=0;j<MatrizPrioridade[i].length;j++){
                MatrizPrioridade [i][j] = array[j][i];
            }
        }    
        return MatrizPrioridade;
    }
    
    /**
     * Método que calcula os elementos de um vetor através da multiplicação de uma matriz por outro vetor
     * @param MatrizPrioridade, array bidimensional dado
     * @param vecCrit, array monodimensional dado
     * @return array monodimensional vec_pc
     */
    
    public static double[] vetorPrioridadeComp(double[][] MatrizPrioridade, double[] vecCrit){
           double[] vec_pc= new double[MatrizPrioridade.length];
           double soma=0;
        for(int i=0;i<MatrizPrioridade.length;i++){
            soma=0;
            for(int k=0;k<MatrizPrioridade[0].length;k++){
               soma=soma+((MatrizPrioridade[i][k])*(vecCrit[k])); 
            }
            if(soma<0){
                soma=soma*(-1);
            }
            vec_pc[i]=soma;
            
        }
        return vec_pc;
    }
    
    /**
     * Método que encontra o valor máximo num array monodimensional
     * @param vec_pc, vetor dado
     * @return string "A alternativa n mais é a mais viável", sendo que n é a posição do valor máximo do vetor 
     */
    
    public static String findMax(double[] vec_pc){
    double maior=0;
        int c1=0,c2=0;
        String n="",solucao;
        for(int k=0;k<vec_pc.length;k++){
            if(vec_pc[k]>maior){
                maior=vec_pc[k];
            }
            
        }
        for(int j=0;j<vec_pc.length;j++){
            if(vec_pc[j]==maior){
                c1++;
            }
        }
        for(int j=0;j<vec_pc.length;j++){
            if(vec_pc[j]==maior){
                c2++;
                if(c2==c1){
                    n = n + ""+(j+1);
                }else if(c2==c1-1){
                    n = n + "" + (j+1) + " e ";
                }else{
                    n = n + "" +(j+1) +", ";
                }
            }
        }

        if(c1>1){
            solucao="As alternativas "+n+" são igualmente viáveis";
        }else{
            solucao="A alternativa "+n+" é a mais viável";
        }
        return solucao;
    }
    
    /**
     * Verifica se uma matriz cumpre a regra de Saaty
     * @param array, matriz a verificar
     * @return true se cumprir a regra, false se não cumprir
     */
    public static boolean regraSaaty(double[][] array){
        
        for(int i=0;i<array.length;i++){
            for(int j=0;j<array[i].length;j++){
                if(array[i][j]!=(1/array[j][i])){
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
     * Imprime as alternativas, uma matriz, o vetor prioridade relativo a essa matriz, o valor próprio associado ao vetor, o valor IR e o valor RC
     * @param fout, Formatter associado a um ficheiro de texto ou à consola
     * @param altCrit, Matriz de Strings com os nomes dos critérios e das alternativas
     * @param n_crit, número de critérios
     * @param n_alt, número de alternativas
     * @param i, posição dos arrays a imprimir
     * @param critMatrixNorm, Matrizes de comparação das alternativas normalizadas
     * @param critVectorV, Vetores prioridade relativos às matrizes de comparação das alternativas
     * @param maxeigen_valueV, Valores próprios máximos associados aos vetores
     * @param RCv, Valores de RC associados às matrizes 
     * @param c Strign que controla o método printVetor
     */
    public static void print_Alt_Data(Formatter fout, String[][] altCrit, int n_crit, int n_alt,int i,double[][][] critMatrixNorm,double [][] critVectorV,double [] maxeigen_valueV, double [] RCv,String c) {
        fout.format(altCrit[0][i]+":");
        fout.format("%n");
        Matrizes_Vetores.printAltCrit(altCrit,1,fout,n_crit,n_alt);
        fout.format("%n");
        Matrizes_Vetores.printMatriz(fout,critMatrixNorm[i]);
        fout.format("%n");
        fout.format("Vetor Prioridade Relativa:");
        fout.format("%n");
        Matrizes_Vetores.printVetor(fout,critVectorV[i],c);
        fout.format("%n");
        fout.format("%-15.20s","Valor Próprio="+maxeigen_valueV[i]);
        fout.format("%-15.11s","   IR="+AHP.TABELA_IR[n_alt-1]);
        fout.format("%-15.11s","   RC="+RCv[i]);
    }
    
    /**
     * Método que verifica se os valores de RC das matrizes são superiores do limiar definido e se o peso de cada critério é inferior ao limiar estabelecido;
     * o método pára o programa se os valores de RC forem superiores ao limiar e descarta qualquer critério cujo peso seja inferior ao limiar
     * @param RC, valor do RC da matriz de comparação dos critérios
     * @param RCv, valores de RC das matrizes de comparação das alternativas
     * @param critVector, vetor que contém os pesos dos critérios
     * @param limiar_atrib, limiar referente aos pesos dos critérios; este limiar é introduzido pelo utilizador, caso não seja o seu valor é de 0
     * @param limiar_RC, limiar referente aos valores de RC; este limiar é introduzido pelo utilizador, caso não o seja o seu valor é de 0.1
     * @param n_crit, nº de critérios
     * @param crit_Elim vector que guarda os critérios eliminados
     * @param altCrit_AHP matriz que contémos nomes dos critérios e das alternativas
     */
    public static void limiarCritRC(double RC,double[] RCv,double[] critVector,double limiar_atrib,double limiar_RC,int n_crit,String[] crit_Elim,String[][] altCrit_AHP){
        
        double a=0;
        int c=0;
        
        if(RC>limiar_RC){
            System.out.println("O RC da matriz dos critérios está abaixo do limiar imposto. O programa agora vai terminar.");
            System.exit(0);
        }else for(int i=0;i<RCv.length;i++){
            if(RCv[i]>limiar_RC){
                System.out.println("O RC da matriz dos critérios está abaixo do limiar imposto. O programa agora vai terminar.");
                System.exit(0);
            }
        }
        
        for(int k=0;k<critVector.length;k++){
           if(critVector[k]<limiar_atrib){
               a+=critVector[k];
               critVector[k]=0;
               crit_Elim[c]=altCrit_AHP[0][k];
               c++;
           } 
        }
        for(int i=0;i<critVector.length;i++){
            if(critVector[i]!=0){
                critVector[i]+=a/(n_crit-c);
            }
        }
    }
    
    /**
     * Normaliza um vetor
     * @param array, vetor a normalizar
     * @return vetor normalizado
     */
    public static double [] normalizar_Vetor(double[] array){
        double soma=0;
        for(int i=0;i<array.length;i++){
            soma += array[i];
        }
        for(int i=0;i<array.length;i++){
            array[i]=array[i]/soma;
        }
        return array;
    }
}