
package lapr1;

import java.util.Formatter;

//Classe com métodos que envolvem tratamento de dados em arrays mono e bidimensionais

public class Matrizes_Vetores {
    public static final String C = "-";
    public static final String ZERO = "0";
    public static final String ELIM = "";
    
    /**
     * Este método trata strings que tenham nºs fracionários e devolve o resultado da fração em double
     * @param l, string de um nº fracionário ex: "1/2"
     * @return nº fracionário em double ex: "1/2" -> 0.5
     */ 
    public static double fraction(String l){
        if(l.contains("/")){
            String [] fraction = l.split("/");
            double num = Double.parseDouble(fraction[0])/Double.parseDouble(fraction[1]);
            return num;
        }
        return Double.parseDouble(l);
    }
    
    /**
     * Este método preenche uma dada linha de uma matriz
     * @param matrix, matriz que se pretende preencher
     * @param l, vetor de strings com os elementos que vão preencher uma linha da matriz
     * @param i, linha da matriz que se pretende preencher
     */
    
    public static void preencherLinha(double [][] matrix, String [] l,int i){
        for(int j=0;j<l.length;j++){
            matrix[i][j]=fraction(l[j]);
        }
    }
    
    /**
     * Este método copia os elementos uma matriz para uma cópia da mesma
     * @param array
     * @param arrayCopy
     */
    public static void criarCopiaMatriz(double[][] array, double[][] arrayCopy){
        for(int i=0;i<array.length;i++){
            System.arraycopy(array[i], 0, arrayCopy[i], 0, array[0].length);
        }  
    }
    
    /**
     * Este método imprime uma dada matriz
     * @param fout, formatter associado a um ficheiro txt ou à consola
     * @param matriz , matriz dada para ser impressa
     */
    
    public static void printMatriz(Formatter fout, double[][] matriz){
        for(int i=0;i<matriz.length;i++){
            for(int j=0;j<matriz[i].length;j++){
                fout.format("%-15.4s",matriz[i][j]+" ");
            }
            fout.format("%n");
        }
    }
    
    /**
     * Este método imprime um dado vetor 
     * @param fout, formatter associado a um ficheiro txt ou à consola
     * @param vector, vetor dado para ser impresso 
     * @param c, String que controla o método em uso
     */
    public static void printVetor(Formatter fout, double[] vector, String c){
        for(int i=0;i<vector.length;i++){
            if(vector[i]==0){
                fout.format("%-15.4s",c);
            }else{
                fout.format("%-15.4s",vector[i]);
            }    
            fout.format("%n");
        }
    }
    
    /**
     * Método que imprime os nomes dos critérios ou os nomes das alternativas
     * @param altCrit, array que contém os nomes dos critérios na 1ª linha e o nome das alternativas na 2ª linha
     * @param i, linha a imprimir do array altCrit
     * @param fout, Formatter associado a um ficheiro de texto ou à consola
     * @param n_crit, nº de critérios
     * @param n_alt, nº de alternativas 
     */
    public static void printAltCrit(String[][] altCrit, int i, Formatter fout,int n_crit, int n_alt){
        if(i==0){
            for(int j=0;j<n_crit;j++){
                fout.format("%-15s",altCrit[i][j]);
            }
        }else if (i==1){
            for(int j=0;j<n_alt;j++){
                fout.format("%-15s",altCrit[i][j]);
            }
        }
    }

     /**
     * Método que imprime informação de um array tridimensional
     * @param fout, Formatter associado a um ficheiro de texto ou à consola
     * @param altCrit, array que contém os nomes dos critérios e das alternativas
     * @param n_crit, nº de critérios
     * @param n_alt, nº de alternativas
     * @param i, posição da informação relativa à matriz do array tridimensional a imprimir
     * @param critMatrix 
     */
    public static void print_array3D(Formatter fout, String[][] altCrit, int n_crit, int n_alt,int i,double [][][] critMatrix) {
        fout.format(altCrit[0][i]+":");
        fout.format("%n");
        Matrizes_Vetores.printAltCrit(altCrit,1,fout,n_crit,n_alt);
        fout.format("%n");
        Matrizes_Vetores.printMatriz(fout,critMatrix[i]);
    }
    
    /**
     * Método que verifica se algum valor dos arrays preenchidos no método de input é negativo
     * @param array, array preenchido no método de input
     * @return false se existir algum valor negativo no array; true não existirem valores negativos no array
     */
     public static boolean num_Neg(double[][] array){
        for(int i=0; i<array.length;i++){
            for(int j=0;j<array[i].length;j++){
                if(array[i][j]<0){
                    return false;
                }
            }
        }
        return true;
    }
     
     /**
      * Método que preenche um vector com Strings ""
      * @param crit_Elim vetor de critérios eliminados
      * @param n_crit nº de critérios
      */
     
     public static void preencherElim(String[] crit_Elim,int n_crit){
         for(int i=0;i<n_crit;i++){
             crit_Elim[i]=ELIM;
         }
     }
}

