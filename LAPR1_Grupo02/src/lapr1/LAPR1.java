
package lapr1;

import java.util.Scanner;
import java.io.FileNotFoundException;
import java.util.Formatter;

//Classe principal da aplicação, aqui é feita a leitura dos dados de input e o output para a consola e para o ficheiro de texto

public class LAPR1 {

   //Variáveis globais da classe
    private final static Scanner IN = new Scanner(System.in);
    private static final String LIMIAR_RC = "-S";
    private static final String LIMIAR_CRT = "-L";
    private static final String PARAM_METODO = "-M";
    private static final String CALCULO_APROX = "a";
    private static final String CALCULO_EXATO = "e";
    private static final String EMPTY = "";
    
    
    public static void main(String[] args) throws FileNotFoundException{
        
     Formatter erros = LogErros.criarEAbrirFileLogErros(LogErros.FILE_LOG_ERROS_LAPR1);
        
        String fich_input=EMPTY;
        String fich_output=EMPTY;
        String param;
        double limiar_atrib=0, limiar_RC=0.10;
       
        if(args.length==4 || args.length==6 || args.length==8){
            if(!args[0].equals(PARAM_METODO)){
                LogErros.registo_NomeParam(args[0], erros);
                System.out.println("Ocorreram erros, consulte o ficheiro " + LogErros.FILE_LOG_ERROS_LAPR1 + " para ver detalhes.");
                LogErros.fecharLogErros(erros);
                System.exit(0);
            }
            param = args[1];
            int n_crit,n_alt;

            if(param.equals("1")){
                
//                Este case verifica a existência do limiar introduzido pelo utilizador
                switch (args.length) {
                    //Se não houver limiares L e S
                    case 4:
                        fich_input = args[2];
                        fich_output = args[3];
                        break;
                    //Se houver limiar L ou S    
                    case 6:
                        if(args[2].equals(LIMIAR_CRT)){
                            limiar_atrib=Double.parseDouble(args[3]);  
                        }else if(args[2].equals(LIMIAR_RC)){
                            limiar_RC=Double.parseDouble(args[3]);
                        }else{
                            System.out.println("Ocorreram erros, consulte o ficheiro " + LogErros.FILE_LOG_ERROS_LAPR1 + " para ver detalhes.");
                            LogErros.registo_NomeParam(args[2], erros);
                            LogErros.fecharLogErros(erros);
                            System.exit(0);
                        }
                        fich_input = args[4];
                        fich_output = args[5];
                        break;
                    //Se houver ambos limiares L e S
                    default:
                        if(args[2].equals(LIMIAR_CRT) && args[4].equals(LIMIAR_RC)){
                            limiar_atrib=Double.parseDouble(args[3]);
                            limiar_RC=Double.parseDouble(args[5]);
                            fich_input = args[6];
                            fich_output = args[7];
                        }else{
                            System.out.println("Ocorreram erros, consulte o ficheiro " + LogErros.FILE_LOG_ERROS_LAPR1 + " para ver detalhes.");
                            LogErros.registo_NomeParam(args[2]+ " e " +args[4], erros);
                            LogErros.fecharLogErros(erros);
                            System.exit(0);
                        }
                        break;
                }
                String[][] altCrit_AHP=File_Input.read_Alt_Crit_AHP(fich_input,erros);
                n_crit= altCrit_AHP[0].length;
                n_alt = altCrit_AHP[1].length;
                
                double maxeigen_value=0;
                double RC;
                double IC;
                double [][] critM = new double [n_crit][n_crit];
                double [] [] critMNorm = new double [n_crit][n_crit];
                double [] [] [] critMatrixNorm = new double [n_crit][n_alt][n_alt];
                //É no critVector que é preciso verificar se há valores menores que o limiar. 
                //Apresentar os atributos eliminados no ficheiro e na consola.
                double [] critVector = new double [n_crit];
                double [][] critVectorV = new double [n_crit][n_alt];
                double [][] MatrizPrioridades;
                double [] PrioridadeComposta;
                double [][][] critMatrix = new double [n_crit][n_alt][n_alt];
                double [] maxeigen_valueV = new double [n_crit];
                double [] ICv = new double [n_crit];
                //Se houver algum RC em RCv acima do limiar RC, informar o utilizador que a matriz i tem um RC acima do limiar
                double [] RCv = new double [n_crit];
                String [] crit_Elim = new String [n_crit];
                
                Matrizes_Vetores.preencherElim(crit_Elim,n_crit);
                
                File_Input.read(fich_input,n_crit,n_alt,critMatrix,critM);
                
                if(!Matrizes_Vetores.num_Neg(critM)){
                    System.out.println("Ocorreram erros, consulte o ficheiro " + LogErros.FILE_LOG_ERROS_LAPR1 + " para ver detalhes.");
                    LogErros.registo_Num_Invalido(critM, erros);
                    LogErros.fecharLogErros(erros);
                    System.exit(0);
                }
                for(int i=0;i<critMatrix.length;i++){
                    if(!Matrizes_Vetores.num_Neg(critMatrix[i])){
                        System.out.println("Ocorreram erros, consulte o ficheiro " + LogErros.FILE_LOG_ERROS_LAPR1 + " para ver detalhes.");
                        LogErros.registo_Num_Invalido(critMatrix[i], erros);
                        LogErros.fecharLogErros(erros);
                        System.exit(0);
                    }
                }

                if(!AHP.regraSaaty(critM)){
                    System.out.println("Ocorreram erros, consulte o ficheiro " + LogErros.FILE_LOG_ERROS_LAPR1 + " para ver detalhes.");
                    LogErros.registo_Saaty(critM, erros);
                    LogErros.fecharLogErros(erros);
                    System.exit(0);
                }             
                for(int i=0;i<n_crit;i++){
                    if(!AHP.regraSaaty(critMatrix[i])){
                        System.out.println("Ocorreram erros, consulte o ficheiro " + LogErros.FILE_LOG_ERROS_LAPR1 + " para ver detalhes.");
                        LogErros.registo_Saaty(critMatrix[i], erros);
                        LogErros.fecharLogErros(erros);
                        System.exit(0);
                    }
                }
            
                System.out.println("Prefere o cálculo exato (" + CALCULO_EXATO + ") dos valores e vectores próprios ou o cálculo aproximado(" + CALCULO_APROX + ")?");
                String esc=IN.nextLine();
                if(!esc.equals(CALCULO_APROX) && !esc.equals(CALCULO_EXATO)){
                    System.out.println("Ocorreram erros, consulte o ficheiro " + LogErros.FILE_LOG_ERROS_LAPR1 + " para ver detalhes.");
                    LogErros.registo_Input_Invalido(esc, erros);
                    LogErros.fecharLogErros(erros);
                    System.exit(0);
                }
            
                Matrizes_Vetores.criarCopiaMatriz(critM,critMNorm);
                for(int i=0;i<n_crit;i++){
                    Matrizes_Vetores.criarCopiaMatriz(critMatrix[i],critMatrixNorm[i]);
                }
                
                AHP.normalizarMatriz(critMNorm);
                for(int i=0;i<n_crit;i++){
                    AHP.normalizarMatriz(critMatrixNorm[i]);
                }
            
        
                if(CALCULO_EXATO.equals(esc.trim())){
                    maxeigen_value=AHP.maxEigenValueExato(critM);
                    for(int i=0;i<n_crit;i++){
                        maxeigen_valueV [i]=AHP.maxEigenValueExato(critMatrix[i]);
                    }
                    critVector=AHP.EigenVectorExato(critM);
                    for(int i=0;i<n_crit;i++){
                        critVectorV[i]=AHP.EigenVectorExato(critMatrix[i]);
                    }
                }else if(CALCULO_APROX.equals(esc.trim())){
                    critVector=AHP.EigenVectorAprox(critMNorm);
                    for(int i=0;i<n_crit;i++){
                        critVectorV[i]=AHP.EigenVectorAprox(critMatrixNorm[i]);
                    }
                    maxeigen_value=AHP.maxEigenValueAprox(critM,critVector);
                    for(int i=0;i<n_crit;i++){
                        maxeigen_valueV [i]=AHP.maxEigenValueAprox(critMatrix[i],critVectorV[i]);
                    }
                }
        
        
                IC = AHP.calcularIC(maxeigen_value, critM);
                if(IC<0){
                    IC=IC*(-1);
                }
                for(int i=0;i<n_crit;i++){
                    ICv[i]=AHP.calcularIC(maxeigen_valueV[i],critMatrix[i]);
                    if(ICv[i]<0){
                        ICv[i]=ICv[i]*(-1);
                    }
                }

                RC = AHP.calcularRC(IC,critM,n_crit,n_alt);
                for(int i=0;i<n_crit;i++){
                    RCv[i]=AHP.calcularRC(ICv[i], critMatrix[i],n_crit,n_alt);
                }

                AHP.limiarCritRC(RC,RCv,critVector,limiar_atrib,limiar_RC,n_crit,crit_Elim,altCrit_AHP);
                
                MatrizPrioridades = AHP.matrizPrioridade(critVectorV);
                PrioridadeComposta = AHP.vetorPrioridadeComp(MatrizPrioridades, critVector);
                String solucao = AHP.findMax(PrioridadeComposta);
                
                Output_Res.output_AHP_ficheiro(altCrit_AHP,fich_output,critMatrix,critM, critMatrixNorm, critMNorm, maxeigen_value,maxeigen_valueV,IC,ICv,RC,RCv,critVector, critVectorV, PrioridadeComposta,solucao,n_crit,n_alt,crit_Elim);
                Output_Res.output_AHP_consola(altCrit_AHP,critMatrix,critM,PrioridadeComposta,solucao,n_crit,n_alt,crit_Elim);
            
            }else if(param.equals("2") && args.length==4){

                fich_input = args[2];
                fich_output = args[3];
                
                String[][] crit_ben_custo = new String[2][];
                String[][] altCrit_TOPSIS = new String[2][];
                
                File_Input.read_Alt_Crit_TOPSIS(altCrit_TOPSIS,fich_input,erros);
                
                n_crit=altCrit_TOPSIS[0].length;
                n_alt=altCrit_TOPSIS[1].length;
                
                double[][][] array_TOPSIS = new double[3][n_alt][n_crit];
                double[] vetor_pesos = new double[n_crit];
                double[] sol_ideal;
                double[] sol_id_neg;
                double[] dist_sol_ideal;
                double[] dist_sol_id_neg;
                double[] rprox_rel;
                int[] pos_crit = new int[altCrit_TOPSIS[0].length];
                crit_ben_custo = new String[2][altCrit_TOPSIS[0].length];
                int alt_esc;
                
                File_Input.preenchernum_TOPSIS(pos_crit,altCrit_TOPSIS,crit_ben_custo,fich_input,array_TOPSIS,vetor_pesos);

                if(!Matrizes_Vetores.num_Neg(array_TOPSIS[0])){
                    System.out.println("Ocorreram erros, consulte o ficheiro " + LogErros.FILE_LOG_ERROS_LAPR1 + " para ver detalhes.");
                        LogErros.registo_Num_Invalido(array_TOPSIS[0], erros);
                        LogErros.fecharLogErros(erros);
                        System.exit(0);
                }
                TOPSIS.preencher_pesos(vetor_pesos,n_crit);
                Matrizes_Vetores.criarCopiaMatriz(array_TOPSIS[0], array_TOPSIS[1]);
                array_TOPSIS[1]=TOPSIS.normalizarMatrizTOPSIS(array_TOPSIS[1]);
                Matrizes_Vetores.criarCopiaMatriz(array_TOPSIS[1], array_TOPSIS[2]);
                array_TOPSIS[2]=TOPSIS.pesarMatrizNormalizada(array_TOPSIS[2], vetor_pesos);
                sol_ideal = TOPSIS.solucaoIdeal(array_TOPSIS[2], pos_crit);
                sol_id_neg = TOPSIS.solucaoIdealNegativa(array_TOPSIS[2], pos_crit);
                dist_sol_ideal=TOPSIS.calcularDistancias(array_TOPSIS[2], sol_ideal);
                dist_sol_id_neg=TOPSIS.calcularDistancias(array_TOPSIS[2], sol_id_neg);
                rprox_rel = TOPSIS.racioProximidadeAlternativa(dist_sol_id_neg, dist_sol_ideal);
                alt_esc = TOPSIS.alternativaEscolhida(rprox_rel);
                Output_Res.output_TOPSIS_fich(altCrit_TOPSIS,fich_output,vetor_pesos,array_TOPSIS,sol_ideal,sol_id_neg,dist_sol_ideal,dist_sol_id_neg,rprox_rel,alt_esc);    
                Output_Res.output_TOPSIS_consola(altCrit_TOPSIS,vetor_pesos,array_TOPSIS,rprox_rel,alt_esc);
            }else{
                System.out.println("Ocorreram erros, consulte o ficheiro " + LogErros.FILE_LOG_ERROS_LAPR1 + " para ver detalhes.");
                LogErros.registo_NParam(args.length, erros);
            }
        }else{
            LogErros.registo_NParam(args.length, erros);
            System.out.println("Ocorreram erros, consulte o ficheiro " + LogErros.FILE_LOG_ERROS_LAPR1 + " para ver detalhes.");
        }
        LogErros.fecharLogErros(erros);
    }
}
    
    
