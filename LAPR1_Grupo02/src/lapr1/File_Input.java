
package lapr1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;


public class File_Input {
    
    //Método de leitura do ficheiro txt com a informação de input
    
    private static final String MC_AHP = "mc";
    private static final String CRT_TOPSIS = "crt ";
    private static final String ALT = "alt";
    private static final String VEC_TOPSIS = "vec";
    private static final String CRT_CUSTO_TOPSIS = "crt_custo";
    private static final String CRT_BEN_TOPSIS = "crt_beneficio";
    private static final String DBL_SPACE = "  ";
    private static final String MD_CRT_ALT_TOPSIS = "md_crt_alt";
    
    
     /**
     * Método que lê o ficheiro de Input do método AHP e preenche as matrizes de comparação dos critérios e das alternativas
     * @param fich_input, nome do ficheiro de input do método AHP
     * @param n_crit, nº de critérios
     * @param n_alt, nº de alternativas
     * @param critMatrix, array que contém as matrizes de comparação das alternativas
     * @param critM, matriz de comparação dos critérios
     * @throws FileNotFoundException, Caso o ficheiro não seja encontrado
     */
    public static void read(String fich_input,int n_crit,int n_alt, double [][][] critMatrix, double [][] critM)throws FileNotFoundException{
        Scanner file=new Scanner(new File(fich_input));
        int z=0,y;
        for(int i=0; i<n_crit+1;i++){
            y=0;
            if(i==0){
                while(file.hasNext()){
                    String line =file.nextLine().trim();
                    if(line.length()>0){
                        if(line.startsWith(MC_AHP) && z==0){
                            line=file.nextLine().trim();
                            z++;
                        }else if(line.startsWith(MC_AHP) && z==1){
                            break;
                        }
                        line=line.replaceAll(" +", " ");
                        String [] l =line.split(" ");
                        Matrizes_Vetores.preencherLinha(critM, l, y);
                        y++;
                    }
                }
            }else{
                while(file.hasNext()){
                    String line =file.nextLine().trim();
                    if(line.length()>0){
                        if(line.startsWith(MC_AHP) && z==1){
                            break;
                        }
                        line=line.replaceAll(" +", " ");
                        String [] l =line.split(" ");
                        Matrizes_Vetores.preencherLinha(critMatrix[i-1], l, y);
                        y++;
                    }
                } 
            }
        }
        
        
        file.close();
    }
    
    /**
     * Método que lê o ficheiro de Input do método AHP e preenche o array que contém os nomes dos critérios e das alternativas
     * @param fich_input, nome do ficheiro de Input do método AHP
     * @param erros, Formatter que regista erros encontrados no ficheiro
     * @return array que contém os nomes dos critérios e das alternativas
     * @throws FileNotFoundException, Caso o ficheiro não seja encontrado 
     */
    public static String [][] read_Alt_Crit_AHP(String fich_input, Formatter erros)throws FileNotFoundException{
        Scanner file=new Scanner(new File(fich_input));
        String [][] altCrit =new String[2][];
        int c=0;
        while(file.hasNext() && c!=2){
            String line=file.nextLine().trim();
            if(line.length()>0){
                if(!line.startsWith(MC_AHP) && !Character.isDigit(line.charAt(0))){
                    System.out.println("Ocorreram erros, consulte o ficheiro " + LogErros.FILE_LOG_ERROS_LAPR1 + " para ver detalhes.");
                    LogErros.registo_Linha(line, erros);
                    LogErros.fecharLogErros(erros);
                    System.exit(0);
                }
            }
            if(line.startsWith(MC_AHP)){
                String n = line.substring(line.indexOf(DBL_SPACE)).trim();
                altCrit[c]=n.split(DBL_SPACE);
                c++;
            }
        }
        file.close();
        return altCrit;
    }
    
    
    /**
     * Método de lê o ficheiro de Input do método TOPSIS e preenche o array que contém os nomes dos critérios e das alternativas
     * @param altCrit, array que contém os nomes dos critérios e das alternativas
     * @param fich_input, nome do ficheiro de Input do método TOPSIS
     * @param erros, Formatter que regista erros encontrados no ficheiro
     * @throws FileNotFoundException, Caso o ficheiro não seja encontrado
     */
    public static void read_Alt_Crit_TOPSIS(String[][] altCrit, String fich_input,Formatter erros) throws FileNotFoundException{
        
        Scanner file=new Scanner(new File(fich_input));
        String pos_Crit,alt;
        
        while(file.hasNext()){
            String line = file.nextLine().trim();
            if(line.length()>0){
                //                                                                                                        !line.substring(0,1).matches("[0-9]")
                if(!line.startsWith(CRT_BEN_TOPSIS) && !line.startsWith(CRT_CUSTO_TOPSIS) && !line.startsWith(ALT) && !line.startsWith(VEC_TOPSIS) && !line.startsWith(MD_CRT_ALT_TOPSIS) && !line.startsWith(CRT_TOPSIS) && !Character.isDigit(line.charAt(0))){
                    System.out.println("Ocorreram erros, consulte o ficheiro " + LogErros.FILE_LOG_ERROS_LAPR1 + " para ver detalhes.");
                    LogErros.registo_Linha(line, erros);
                    LogErros.fecharLogErros(erros);
                    System.exit(0);
                }
                
                if(line.startsWith(CRT_TOPSIS)){
                    int i = line.indexOf(DBL_SPACE);
                    pos_Crit = line.substring(i+2);
                    String[] temp = pos_Crit.split(DBL_SPACE);
                    altCrit[0]=temp;
                }
                if(line.startsWith(ALT)){
                    int i = line.indexOf(DBL_SPACE);
                    alt = line.substring(i+2);
                    altCrit[1]=alt.split(DBL_SPACE);     
                }
                
            }
        }
        file.close();
    }
    
    
    /**
     * Método que lê o ficheiro de Input do método TOPSIS e que preenche o vetor pesos e a matriz de decisão
     * @param pos_crit, vetor que guarda a qualidade de um critério (benefício=1/custo=-1)
     * @param altCrit_TOPSIS, array que contém os nomes dos critérios e das alternativas
     * @param crit_ben_custo, matriz que guarda os nomes dos critérios de benefícios na 1ª linha e os nomes dos critérios de custo na 2ª linha
     * @param fich_input, nome do ficheiro de Input do método TOPSIS
     * @param array_TOPSIS, array que contém a matrize de decisão de entrada, a matriz de decisão normalizada e a matriz de decisão normalizada pesada; neste método só é preenchida a matriz de decisão de entrada
     * @param vetor_pesos, vetor que contém os pesos dos critérios
     * @throws FileNotFoundException , Caso o ficheiro não seja encontrado
     */
    public static void preenchernum_TOPSIS(int[] pos_crit,String[][] altCrit_TOPSIS,String[][] crit_ben_custo,String fich_input,double[][][] array_TOPSIS,double[] vetor_pesos) throws FileNotFoundException{
        Scanner file=new Scanner(new File(fich_input));
        String critB,critC, pos_Crit;
        crit_ben_custo = new String[2][altCrit_TOPSIS[0].length];
        
        while(file.hasNext()){
            String line = file.nextLine().trim();
            
            if(line.length()>0){
                if(line.startsWith(CRT_BEN_TOPSIS)){
                    int i = line.indexOf(DBL_SPACE);
                    critB = line.substring(i+2);
                    crit_ben_custo[0] = critB.split(DBL_SPACE);
                }else if(line.startsWith(CRT_CUSTO_TOPSIS)){
                    int i = line.indexOf(DBL_SPACE);
                    critC = line.substring(i+2);
                    crit_ben_custo[1] = critC.split(DBL_SPACE);
                }
                
                if(line.startsWith(CRT_TOPSIS)){
                    int i = line.indexOf(DBL_SPACE);
                    pos_Crit = line.substring(i+2);
                    String[] temp = pos_Crit.split(DBL_SPACE);
                    for(int j=0;j<pos_crit.length;j++){
                        for(int k=0; k<crit_ben_custo.length;k++){
                            for(int l=0;l<crit_ben_custo[k].length;l++){
                                if(temp[j].equals(crit_ben_custo[k][l]) && k==0){
                                    pos_crit[j]=1;
                                }else if (temp[j].equals(crit_ben_custo[k][l]) && k==1){
                                    pos_crit[j]=-1;
                                }
                            }
                        }
                        
                    }
                    
                }
                if(line.startsWith(VEC_TOPSIS)){
                    line = file.nextLine();
                    line = line.replaceAll(" +", " ");
                    String[] temp1 = line.split(" ");
                    for(int i=0;i<temp1.length;i++){
                        vetor_pesos[i] = Double.parseDouble(temp1[i]);
                    }
                }
                int c=0;
                if(line.startsWith(ALT)){
                    while(file.hasNext()){
                     line = file.nextLine();
                     line = line.replaceAll(" +", " ");
                     String[] temp1 = line.split(" ");
                     for(int i=0;i<temp1.length;i++){
                        array_TOPSIS[0][c][i] = Double.parseDouble(temp1[i]);
                        
                    }
                     c++;
                    }   
                }
            }
        }
        file.close();
    }
}
