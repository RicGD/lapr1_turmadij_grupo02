
package lapr1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;


public class Output_Res { 
    /**
     * Método que escreve todo o output pedido para um ficheiro txt relativamente ao método AHP
     * @param altCrit, matriz que tem uma linha para as alternativas e outra para os critérios
     * @param fich_output, String do ficheiro de output
     * @param critMatrix, matriz tridimensional que tem as matrizes de comparação das alternativas
     * @param critM, matriz dos critérios
     * @param critMatrixNorm, matriz tridimensional que tem as matrizes de comparação das alternativas normalizada
     * @param critMNorm, matriz dos critérios normalizada
     * @param maxeigen_value, valor próprio máximo da matriz dos critérios
     * @param maxeigen_valueV, vetor próprio máximo da matriz de comparação de alternativas
     * @param IC, indice de consistência da matriz de comparação de critérios
     * @param ICv, vetor dos indices de consistência da matriz de comparação de alternativas
     * @param RC, razão de consistência da matriz de comparação de critérios
     * @param RCv, vetor das razões de consistências da matriz de comparação de alternativas
     * @param critVector, vetor próprio da matriz de comparação de critérios
     * @param critVectorV, matriz com os vetores próprios da matriz de comparação de alternativas
     * @param PrioridadeComposta, vetor da prioridade composta
     * @param solucao, String com a(s) alternativa(s) escolhida(s) 
     * @param n_crit, numero de critérios
     * @param n_alt, numero de alternativas
     * @param crit_Elim vetor com os critérios eliminados
     * @throws FileNotFoundException 
     */
    
     public static void output_AHP_ficheiro(String[][] altCrit,String fich_output,double[][][] critMatrix, double[][] critM, double[][][] critMatrixNorm, double[][] critMNorm,double maxeigen_value,double[] maxeigen_valueV,double IC,double[] ICv,double RC,double[] RCv,double[] critVector, double[][] critVectorV, double[] PrioridadeComposta,String solucao,int n_crit, int n_alt,String[] crit_Elim) throws FileNotFoundException{
            
        Formatter fout = new Formatter(new File(fich_output));
        
        fout.format("MADMC: Método AHP");
        fout.format("%n");
        fout.format("%n");
        if(!crit_Elim[0].equals(Matrizes_Vetores.ELIM)){
            fout.format("Critérios Eliminados:");
            fout.format("%n");
            for(int k=0;k<crit_Elim.length;k++){
                if(!crit_Elim[k].equals(Matrizes_Vetores.ELIM)){
                    fout.format(crit_Elim[k]);
                    fout.format("%n");
                }
            }
        }
        fout.format("Matrizes de Entrada");
        fout.format("%n");
        fout.format("Critérios:");
        fout.format("%n");
        Matrizes_Vetores.printAltCrit(altCrit, 0, fout,n_crit,n_alt);
        fout.format("%n");
        Matrizes_Vetores.printMatriz(fout,critM);
            
        fout.format("%n");
        for(int i=0;i<n_crit;i++){   
            Matrizes_Vetores.print_array3D(fout, altCrit, n_crit, n_alt,i,critMatrix);     
            fout.format("%n");    
        }    
        
        fout.format("%n");
            
        fout.format("%s%n","Matrizes Normalizadas");
            
        fout.format("%n");
            
        fout.format("Critérios:");
        fout.format("%n");
        Matrizes_Vetores.printAltCrit(altCrit, 0, fout,n_crit,n_alt);
        fout.format("%n");
        Matrizes_Vetores.printMatriz(fout,critMNorm);
        fout.format("%n");
        fout.format("Vetor Prioridade Relativa:");
        fout.format("%n");
        Matrizes_Vetores.printVetor(fout,critVector, Matrizes_Vetores.C);
        fout.format("%n");
        fout.format("%-15.20s","Valor Próprio="+maxeigen_value);
        fout.format("%-15.11s","   IR="+AHP.TABELA_IR[n_crit-1]);
        fout.format("%-15.11s","   RC="+RC);
            
        fout.format("%n");
        fout.format("%n");
        
        for(int i=0;i<n_crit;i++){
            AHP.print_Alt_Data(fout, altCrit, n_crit, n_alt,i,critMatrixNorm,critVectorV,maxeigen_valueV,RCv, Matrizes_Vetores.C);
            fout.format("%n");
            fout.format("%n");
        }    
        
        fout.format("Vector de Prioridade Composta:");
        fout.format("%n");
        Matrizes_Vetores.printVetor(fout,PrioridadeComposta, Matrizes_Vetores.C);
            
        fout.format("%n");
            
        fout.format(solucao);
            
        fout.close();
    }
    
     /**
      * Método que escreve o output pedido para a consola relativamente ao método AHP
      * @param altCrit, matriz que tem uma linha para as alternativas e outra para os critérios
      * @param critMatrix, matriz tridimensional que tem as matrizes de comparação das alternativas
      * @param critM, matriz dos critérios
      * @param PrioridadeComposta, vetor da prioridade composta
      * @param solucao, String com a(s) alternativa(s) escolhida(s) 
      * @param n_crit, numero de critérios
      * @param n_alt, numero de alternativas
     * @param crit_Elim vetor com os critérios eliminados
      */
     
    public static void output_AHP_consola(String[][] altCrit,double[][][] critMatrix,double[][] critM, double[] PrioridadeComposta, String solucao,int n_crit,int n_alt,String[] crit_Elim){
        
        Formatter fout = new Formatter(System.out);
        fout.format("%n");
        fout.format("MADMC: Método AHP");
        fout.format("%n");
        fout.format("%n");
        if(!crit_Elim[0].equals(Matrizes_Vetores.ELIM)){
            fout.format("Critérios Eliminados:");
            fout.format("%n");
            for(int k=0;k<crit_Elim.length;k++){
                if(!crit_Elim[k].equals(Matrizes_Vetores.ELIM)){
                    fout.format(crit_Elim[k]);
                    fout.format("%n");
                }
            }
        }
        fout.format("Matrizes de entrada");
        fout.format("%n");
        fout.format("%n");
        Matrizes_Vetores.printAltCrit(altCrit, 0,fout,n_crit,n_alt);
        fout.format("%n");
        Matrizes_Vetores.printMatriz(fout,critM);
        fout.format("%n");
        for(int i=0;i<n_crit;i++){
            Matrizes_Vetores.print_array3D(fout,altCrit,n_crit,n_alt,i,critMatrix);
            fout.format("%n");
        }
        
        fout.format("%n");
        
        fout.format("Vetor Prioridade Composta");
        fout.format("%n");
        Matrizes_Vetores.printVetor(fout,PrioridadeComposta, Matrizes_Vetores.C);
        fout.format("%n");
        
        fout.format("Solução escolhida:");
        fout.format("%n");
        fout.format(solucao);
        fout.format("%n");
    }
    
    /**
     * Método que escreve o output pedido para um ficheiro txt relativamente ao método TOPSIS
     * @param altCrit_TOPSIS, matriz que tem uma linha para as alternativas e outra para os critérios
     * @param fich_output, String do ficheiro de output
     * @param vetor_pesos, vetor de entrada com o peso de casa critério
     * @param array_TOPSIS, array tridimensional que contêm a matriz de entrada, a matriz de entrada normalizada e a matriz de entrada normalizada pesada
     * @param sol_ideal, vetor com os valores da solução ideal
     * @param sol_id_neg, vetor com os valores da solução ideal negativa
     * @param dist_sol_ideal, vetor com os valores da distancia de cada alternativa à solução ideal
     * @param dist_sol_id_neg, vetor com os valores da distancia de cada alternativa à solução ideal negativa
     * @param rprox_rel, vetor com o racio da proximidade relativa de cada alternativa à solução ideal
     * @param esc, posição da alternativa escolhida
     * @throws FileNotFoundException 
     */
    
    public static void output_TOPSIS_fich(String[][] altCrit_TOPSIS,String fich_output,double[] vetor_pesos,double[][][] array_TOPSIS,double[] sol_ideal,double[] sol_id_neg,double[] dist_sol_ideal,double[] dist_sol_id_neg,double[] rprox_rel,int esc) throws FileNotFoundException{
        Formatter fout = new Formatter(new File(fich_output));
        
        fout.format("MADMC escolhido: Método TOPSIS");
        fout.format("%n");
        fout.format("%n");
        fout.format("Vetor dos pesos");
        fout.format("%n");
        Matrizes_Vetores.printVetor(fout, vetor_pesos,Matrizes_Vetores.ZERO);
        
        fout.format("%n");
        fout.format("Matriz de entrada");
        fout.format("%n");
        Matrizes_Vetores.printMatriz(fout, array_TOPSIS[0]);
        fout.format("%n");
        
        fout.format("%n");
        fout.format("Matriz de entrada normalizada");
        fout.format("%n");
        Matrizes_Vetores.printMatriz(fout, array_TOPSIS[1]);
        fout.format("%n");
        
        fout.format("%n");
        fout.format("Matriz de entrada normalizada pesada");
        fout.format("%n");
        Matrizes_Vetores.printMatriz(fout, array_TOPSIS[2]);
        fout.format("%n");
        
        fout.format("%n");
        fout.format("Solução Ideal");
        fout.format("%n");
        Matrizes_Vetores.printVetor(fout,sol_ideal,Matrizes_Vetores.ZERO);
        fout.format("%n");
        
        fout.format("%n");
        fout.format("Solução Ideal Negativa");
        fout.format("%n");
        Matrizes_Vetores.printVetor(fout,sol_id_neg,Matrizes_Vetores.ZERO);
        fout.format("%n");
        
        fout.format("%n");
        fout.format("Distância das alternativas à solução ideal");
        fout.format("%n");
        Matrizes_Vetores.printVetor(fout, dist_sol_ideal,Matrizes_Vetores.ZERO);
        fout.format("%n");
        
        fout.format("%n");
        fout.format("Distância das alternativas à solução ideal negativa");
        fout.format("%n");
        Matrizes_Vetores.printVetor(fout,dist_sol_id_neg,Matrizes_Vetores.ZERO);
        fout.format("%n");
        
        fout.format("%n");
        fout.format("Vetor das proximidades relativas");
        fout.format("%n");
        Matrizes_Vetores.printVetor(fout,rprox_rel,Matrizes_Vetores.ZERO);
        fout.format("%n");
        
        fout.format("%n");
        fout.format("Alternativa escolhida: "+(esc+1));
        fout.format("%n");
        fout.format(altCrit_TOPSIS[1][esc]);
        
        fout.close();
    }
    
    /**
     * Método que escreve o output pedido para a consola relativamente ao método TOPSIS
     * @param altCrit_TOPSIS, matriz que tem uma linha para as alternativas e outra para os critérios
     * @param vetor_pesos, vetor de entrada com o peso de casa critério
     * @param array_TOPSIS, array tridimensional que contêm a matriz de entrada, a matriz de entrada normalizada e a matriz de entrada normalizada pesada
     * @param rprox_rel, vetor com o racio da proximidade relativa de cada alternativa à solução ideal
     * @param esc, posição da alternativa escolhida*
     * @throws FileNotFoundException 
     */
    
    public static void output_TOPSIS_consola(String[][] altCrit_TOPSIS,double[] vetor_pesos,double[][][] array_TOPSIS,double[] rprox_rel,int esc) throws FileNotFoundException{
        
        Formatter fout = new Formatter(System.out);
        
        fout.format("MADMC escolhido: Método TOPSIS");
        fout.format("%n");
        fout.format("%n");
        fout.format("Vetor dos pesos");
        fout.format("%n");
        Matrizes_Vetores.printVetor(fout, vetor_pesos,Matrizes_Vetores.ZERO);
        
        fout.format("%n");
        fout.format("Matriz de entrada");
        fout.format("%n");
        Matrizes_Vetores.printMatriz(fout, array_TOPSIS[0]);
        fout.format("%n");
        
        fout.format("%n");
        fout.format("Matriz de entrada normalizada pesada");
        fout.format("%n");
        Matrizes_Vetores.printMatriz(fout, array_TOPSIS[2]);
        fout.format("%n");
        
        fout.format("%n");
        fout.format("Vetor das proximidades relativas");
        fout.format("%n");
        Matrizes_Vetores.printVetor(fout,rprox_rel,Matrizes_Vetores.ZERO);
        fout.format("%n");
        
        fout.format("%n");
        fout.format("Alternativa escolhida: "+(esc+1));
        fout.format("%n");
        fout.format(altCrit_TOPSIS[1][esc]);
        fout.format("%n");
    }
}
