
package lapr1;

public class TOPSIS {

    /**
     * Método que normaliza uma matriz
     * @param matriz, matriz a normalizar
     * @return matriz normalizada
     */
    public static double[][] normalizarMatrizTOPSIS(double [][] matriz){
        
        double [] array= new double [matriz[0].length];
        
        double soma=0;
        
        for(int j=0;j<matriz.length;j++){
            
            for(int i=0;i<matriz[0].length;i++){
                
                array[i]=matriz[i][j];
                
            }
            for(int l=0;l<matriz.length;l++){
                
                soma=soma+(array[l]*array[l]);
                
            }
            for(int k=0;k<matriz.length;k++){
                
                array[k]=array[k]/Math.sqrt(soma);
                
                matriz[k][j]=array[k];
                
            }
            
            soma=0;
            
        }
        
        return matriz;
    }
    
    /**
     * Método que multiplica o vetor pesos por uma matriz normalizada
     * @param matriz, matriz normalizada
     * @param array, vetor pesos
     * @return matriz normalizada pesada
     */
    public static double[][] pesarMatrizNormalizada (double[][] matriz, double[] array){
            
        double a=0;
            
        for(int i=0;i<array.length;i++){
            for(int k=0;k<matriz.length;k++){
                a=(matriz[k][i]*array[i]);
                matriz[k][i]=a;
            }
        }
    
        return matriz;
    }
    
    /**
     * Método que pecorre todas as colunas de uma matriz e cria um vetor com o maior valor das colunas referentes a critérios de benefício e com o menor valor das colunas referentes a critérios de custo
     * @param matriz, matriz de decisão normalizada pesada
     * @param pos_crit, vetor que guarda a qualidade de um critério (benefício=1/custo=-1)
     * @return vetor com so valores da solução ideal
     */
    public static double[] solucaoIdeal (double[][] matriz, int [] pos_crit){
        
        double[] vec_SI= new double[matriz[0].length];
        
        
        
        for(int i=0;i<matriz[0].length;i++){
            double z=0;
            double y=11;
            if(pos_crit[i]==1){
                vec_SI[i] = findMax(matriz,i);
            }else if(pos_crit[i]==-1){
                vec_SI[i] = findMin(matriz,i);
                
            }
        }
     return vec_SI;   
    }
    
    
    /**
     * Método que pecorre todas as colunas de uma matriz e cria um vetor com o menor valor das colunas referentes a critérios de benefício e com o maior valor das colunas referentes a critérios de custo
     * @param matriz, matriz de decisão normalizada pesada
     * @param pos_crit, vetor que guarda a qualidade de um critério (benefício=1/custo=-1)
     * @return vetor com so valores da solução ideal negativa
     */
    public static double[] solucaoIdealNegativa (double[][] matriz, int[] pos_crit){
        
        double[] vec_SIN= new double[matriz[0].length];
        
         for(int i=0;i<matriz[0].length;i++){
            double z=0;
            double y=11;
            if(pos_crit[i]==-1){
                vec_SIN[i]=findMax(matriz,i);    
            }else if(pos_crit[i]==1){
                vec_SIN[i]=findMin(matriz,i);
            }
        }
        
        
        
        return vec_SIN;
    }
    
    
    /**
     * Método que encontra o maior valor uma coluna de uma matriz
     * @param matriz, matriz normalizada pesada
     * @param i, coluna da matriz a percorrer
     * @return maior valor da coluna percorrida
     */
    public static double findMax( double[][] matriz, int i){
        
        double max=0;
        
        for(int k=0;k<matriz.length;k++){
            
            if(matriz[k][i]>max){
                max=matriz[k][i];
            }
            
        }
        
        return max;
    }
    
    
    /**
     * Método que encontra o menor valor uma coluna de uma matriz
     * @param matriz, matriz normalizada pesada
     * @param i, coluna da matriz a percorrer
     * @return menor valor da coluna percorrida
     */
    public static double findMin(double[][]matriz,int i){
        
        double min=matriz[0][i];
        
        for(int l=0;l<matriz.length;l++){
            
            if(matriz[l][i]<min){
                min=matriz[l][i];
            }
            
        }
        
        return min;
    }
    
    /**
     * Método que calcula as distências das várias soluções possíveis à solução ideal ou à solução ideal negativa
     * @param matriz, matriz normalizada pesada
     * @param array, vetor que contém ou a solução ideal ou a solução ideal negativa
     * @return vetor com as distâncias das soluções à solução ideal ou à solução ideal negativa
     */
    public static double[] calcularDistancias( double[][] matriz, double[] array){
        
        double[] vec= new double[matriz.length];
        double soma=0;
        
        for(int i=0;i<matriz.length;i++){
            
            for(int k=0;k<matriz[0].length;k++){
               soma=soma+((array[k]-matriz[i][k])*(array[k]-matriz[i][k])); 
            }
            
            vec[i]=Math.sqrt(soma);
            soma=0;
            
        }
        
        return vec;
    }
    
    /**
     * Método que calcula a proximidade realativa de cada alternativa à solução ideal
     * @param vec_DSIN, vetor com as distâncias à solução ideal negativa
     * @param vec_DSI, vetor com as distâncias à solução ideal
     * @return vetor que contém a proximidade relativa de cada alternativa à solução ideal
     */
    public static double[] racioProximidadeAlternativa (double [] vec_DSIN, double [] vec_DSI){
        
        double [] r = new double [vec_DSIN.length];
        
        for(int i=0;i<vec_DSIN.length;i++){
            
            r[i]=(vec_DSIN[i]/(vec_DSI[i]+vec_DSIN[i]));
              
        }
        
        return r;
        
    }
    
    /**
     * Método que percorre um vetor e retorna o valor máximo encontrado
     * @param array, vetor a percorrer
     * @return valor máximo encontrado
     */
    public static int alternativaEscolhida(double[] array){
        
        double max=0;
        int esc=0;
       
        for(int i=0;i<array.length;i++){
            if(array[i]>max){
                max=array[i];
                esc=i;
            }
        }
       
       return esc;
    }
    
    /**
     * Método que preenche o vetor pesos se este não tiver sido preenchido pelo método de leitura
     * @param vec, vetor a preencher
     * @param n_crit, nº de critérios
     */
    public static void preencher_pesos(double[] vec,int n_crit){
        for(int i=0;i<n_crit;i++){
            if(vec[i]==0){
                vec[i]=(double)1/n_crit;
            }
        }
    }
    
}



    
  
