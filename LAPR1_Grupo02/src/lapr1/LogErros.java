
package lapr1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class LogErros {
    
    private final static DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH_mm_ss");
    private final static Date DATE = new Date();
    private final static String DATA_HORA = DATE_FORMAT.format(DATE);
    public final static String FILE_LOG_ERROS_LAPR1 = "ErrosLAPR1 " + DATA_HORA + ".txt";
    public final static String FILE_TESTE_LOG_ERROS_LAPR1 = "ErrosTESTE_LAPR1 " + DATA_HORA + ".txt";
    
    /**
     * Método que cria e abre o Formatter associado ao ficheiro de texto que regista os erros
     * @param nomeFile, String do nome do ficheiro
     * @return Formatter associado ao ficheiro de texto que regista os erros
     * @throws FileNotFoundException 
     */
    
    public static Formatter criarEAbrirFileLogErros(String nomeFile) throws FileNotFoundException{
        Formatter file = new Formatter(new File(nomeFile));
        return file;
    }
    
    /**
     * Método que verifica se foi introduzido o número correto de parâmetros
     * @param args_l, número de parâmetros introduzidos
     * @param file, Formatter associado ao ficheiro de texto que regista os erros
     */
    
    public static void registo_NParam(int args_l,Formatter file){
        data_Erro(DATE,file);
        file.format("   ERRO: Nº de parâmetros introduzidos incorreto (" + args_l + ")");
        file.format("%n");
    }
    
    /**
     * Método que verifica se foi introduzido o nome correto dos parâmetros
     * @param nome, String do(s) nome(s) do(s) parâmetro(s) introduzido(s) incorretamente
     * @param file, Formatter associado ao ficheiro de texto que regista os erros
     */
    
    public static void registo_NomeParam(String nome,Formatter file){
        data_Erro(DATE,file);
        file.format("   ERRO: Nome(s) de parâmetro(s) introduzido(s) incorreto(s) (" + nome + ")");
        file.format("%n");
    }
    
    /**
     * Método que verifica se existe linhas inválidas no ficheiro de entrada
     * @param linha, linha(s) inválida(s) no ficheiro de entrada
     * @param file, Formatter associado ao ficheiro de texto que regista os erros
     */
    
    public static void registo_Linha(String linha,Formatter file){
        data_Erro(DATE,file);
        file.format("   ERRO: O ficheiro apresenta linhas inválidas (" + linha + ")");
        file.format("%n");
    }
    
    /**
     * Método que verifica se a matriz de entrada está de acordo com a regra de Saaty
     * @param array, matriz de entrada
     * @param file, Formatter associado ao ficheiro de texto que regista os erros
     */
    
    public static void registo_Saaty(double [][] array,Formatter file){
        data_Erro(DATE,file);
        file.format("   ERRO: A matriz seguinte não cumpre a regra de Saaty");
        file.format("%n");
        Matrizes_Vetores.printMatriz(file, array);
    }
    
    /**
     * Método que fecha o Formatter associado ao ficheiro de texto que regista os erros
     * @param file, Formatter associado ao ficheiro de texto que regista os erros
     */
    
    public static void fecharLogErros(Formatter file){
        file.close();
    }
    
    /**
     * Método que regista a data e a hora de cada erro encontrado
     * @param date, objecto date que tem a data e a hora de quando foi detetado o erro
     * @param file, Formatter associado ao ficheiro de texto que regista os erros
     */
    
    private static void data_Erro(Date date, Formatter file){
        file.format(DATE_FORMAT.format(date));
    }
    
    /**
     * Método que verifica a existêcia de valores inválidos na matriz de entrada (valores negativos ou irracionais)
     * @param array, matriz de entrada
     * @param file, Formatter associado ao ficheiro de texto que regista os erros
     */
    
    public static void registo_Num_Invalido(double [][] array, Formatter file){
        data_Erro(DATE,file);
        file.format("   ERRO:  A matriz seguinte apresenta valores inválidos ( negativos ou irracionais)");
        file.format("%n");
        Matrizes_Vetores.printMatriz(file, array);
    }
    
    /**
     * Método que verifica se a opção do utilizar em relação ao cálculo (exato ou aproximado) é válida
     * @param input, é a escolha do utilizador em relação ao cálculo (exato ou aproximado)
     * @param file, Formatter associado ao ficheiro de texto que regista os erros
     */
    
    public static void registo_Input_Invalido(String input, Formatter file){
        data_Erro(DATE,file);
        file.format("   ERRO:  A opção introduzida não é válida (" + input + ").");
        file.format("%n");
    }
}

