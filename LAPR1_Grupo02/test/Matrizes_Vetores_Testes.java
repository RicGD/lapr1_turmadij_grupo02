
import lapr1.Matrizes_Vetores;

public class Matrizes_Vetores_Testes {
    
    private final static String FRACAO_TESTES = "1/2";
    private final static double NUM_TESTES = 0.5;
    private final static double[][] MATRIZ1_TESTES = {{1,2,0.3},{2,3,7},{0.9,0.8,6}};
    private final static double[][] COPIA_MATRIZ1_TESTES = new double[MATRIZ1_TESTES.length][MATRIZ1_TESTES.length];   
    private final static String[] L_TESTES = {"1","0.5","3"};
    private final static double[][] MATRIZ_NEG_TESTES = {{1,2,3},{0,3,4},{-1,4,8}};
    
    public static void output(){
        
       //Output dos resultados dos testes e do seu input relativos à classe Matrizes_Vetores
       
       System.out.println("\nTESTES ASSOCIADOS AOS MÉTODOS DA CLASSE Matrizes_Vetores");
       System.out.println("================================================\n");
       System.out.println("Teste associado ao método fraction"
               + "\nDados de entrada: fracao_teste=\"1/2\""
               + "\n                  num_teste=0.5");
       if(teste_fraction(FRACAO_TESTES,NUM_TESTES)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return=false)\n");
       }
       System.out.println("Teste associado ao método preencherElim"
               + "\nDados de entrada: crit_Elim_Testes=[{\"\",\"\",\"\"}]");
       if(teste_preencherElim(Testes.CRIT_ELIM_TESTE)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return=false)\n");
       }
       System.out.println("Teste associado ao método criarCopiaMatriz"
               + "\nDados de entrada: matriz1_teste [{1,2,0.3},{2,3,7},{0.9,0.8,6}]"
               + "\n                  copia_matriz1_teste [{},{},{},{}]");
       if(teste_criarCopiaMatriz(MATRIZ1_TESTES,COPIA_MATRIZ1_TESTES)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resulado esperado não obtido (return=false)\n");
       }
       System.out.println("Teste associado ao método preencherLinha"
               + "\nDados de entrada: l_teste [{\"1\",\"0.5\",\"3\"}]"
               + "\n                  matriz_teste [{1.0, 0.5, 3.0},{2.0, 1.0, 4.0},{0.3333333333333333, 0.25, 1.0}]"
               + "\n                  PreencherLinhaEsperado=1");
       if(teste_preencherLinha(Testes.MATRIZ_TESTE,L_TESTES)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return=false)\n");
       }
              System.out.println("Teste associado ao método num_Neg"
               + "\nDados de entrada: matriz_neg_teste [{{1,2,3},{0,3,4},{-1,4,8}}]");
       if(teste_num_Neg(MATRIZ_NEG_TESTES)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return=false)\n");
       }
      
    }
    
    //Métodos de Teste relativos aos métodos da classe Matrizes_Vetores
   
   /**
    * Método de teste do método Matrizes_Vetores.fraction
    * @param fracao_teste
    * @param num_teste
    * @return true se o resultado esperado for obtido(se num_temp=num_teste), false se acontecer o oposto
    */
   
   private static boolean teste_fraction(String fracao_teste, double num_teste){
       double num_temp = Matrizes_Vetores.fraction(fracao_teste);
       return num_temp==num_teste;
   }
   
   /**
    * Método de teste do método Matrizes_Vetores.preencherElim
    * @param critElim_testes
    * @return true se o resultado esperado for obtido(se critElim_testes=temp), false se acontecer o oposto
    */
   
   private static boolean teste_preencherElim (String[] critElim_testes){
      String[] temp = new String[critElim_testes.length];
      Matrizes_Vetores.preencherElim(temp,critElim_testes.length);
      for(int i=0;i<critElim_testes.length;i++){
          if(!temp[i].equals(critElim_testes[i])){
              return false;
          }
      }
      return true;
   }
   
   /**
    * Método de teste do método Matrizes_Vetores.criarCopiaMatriz
    * @param matriz1
    * @param matriz2
    * @param matriz3
    * @param matriz4
    * @param copia_matriz1
    * @param copia_matriz2
    * @param copia_matriz3
    * @param copia_matriz4
    * @return true se o resultado esperado for obtido (se os elementos das matrizes copias forem todos iguais aos das matrizes originais), false se acontecer o oposto
    */
   
   private static boolean teste_criarCopiaMatriz(double[][] matriz_teste,double[][] matriz_teste_copia){
    Matrizes_Vetores.criarCopiaMatriz(matriz_teste,matriz_teste_copia);
    for(int i=0;i<matriz_teste.length;i++){
        for(int j=0;j<matriz_teste[i].length;j++){
            if(matriz_teste[i][j]!=matriz_teste_copia[i][j]){
                return false;
            }
        }
    }
       return true;
   }
   
   /**
    * Método de teste do método Matrizes_Vetores.preencherLinha
    * @param l
    * @param matrix
    * @param PreencherLinhaEsperado
    * @return true se o resultado esperado for obtido (se Matrizes_Vetores.preencherLinha(matrix,l,temp)=PreencherLinhaEsperado), false se acontecer o oposto
    */
   
    private static boolean teste_preencherLinha(double[][]matrix,String[] l){
        int temp=0;
        double[][] m_temp = new double[matrix.length][matrix.length];
        Matrizes_Vetores.preencherLinha(m_temp, l, temp);
            for(int i=0;i<matrix.length;i++){
                if(matrix[0][i]!=m_temp[0][i]){
                    return false;
                }
            }
        return true;
    }
    
    /**
    * Método de teste do método Matrizes_Vetores.num_Neg
    * @param matriz_neg_testes
    * @return true se o resultado esperado for obtido, false se acontecer o oposto
    */
    
    private static boolean teste_num_Neg(double[][] matriz_neg_teste){
        return !Matrizes_Vetores.num_Neg(matriz_neg_teste);
    }

}
