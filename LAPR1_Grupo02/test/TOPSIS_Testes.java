
import lapr1.TOPSIS;

public class TOPSIS_Testes {
    
    private final static double[][] MATRIZTOPSIS_TESTES={{7, 9, 9, 8},{8, 7, 8, 7},{9, 6, 8, 9},{6, 7, 8, 6}};
    private final static double[][] MATRIZTOPSISNORM_TESTES={{0.4615663313770509, 0.6137949055234262, 0.5447047794019222, 0.5275043787166296}, {0.5275043787166296, 0.47739603762933147, 0.48418202613504197, 0.4615663313770509}, {0.5934424260562083, 0.4091966036822841, 0.48418202613504197, 0.5934424260562083}, {0.3956282840374722, 0.47739603762933147, 0.48418202613504197, 0.3956282840374722}};
    private final static double[][] MATRIZNORMALIZADAPESADA_TESTES={{0.04615663313770509, 0.2455179622093705, 0.16341143382057666, 0.10550087574332592}, {0.05275043787166296, 0.1909584150517326, 0.14525460784051258, 0.09231326627541019}, {0.05934424260562084, 0.16367864147291367, 0.14525460784051258, 0.11868848521124167}, {0.039562828403747224, 0.1909584150517326, 0.14525460784051258, 0.07912565680749445}};
    private final static double[] VEC_SI_TESTES={0.05934424260562084, 0.2455179622093705, 0.16341143382057666, 0.07912565680749445};   
    private final static double[] VEC_SIN_TESTES={0.039562828403747224, 0.16367864147291367, 0.14525460784051258, 0.11868848521124167};
    private final static int COLUNA_TESTES=0;
    private final static double MIN_TESTES=0.039562828403747224;
    private final static double MAX_TESTES=0.05934424260562084;
    private final static double[] VEC_DSI_TESTES={0.029488391230979422, 0.05936165277403139, 0.0926960740246281, 0.06080887158583795};
    private final static double[] VEC_DSIN_TESTES={0.085116015253434, 0.040171523046841684, 0.019781414201873612, 0.04805625284826082};
    private final static double[] R_TESTES={0.7426940888613219, 0.4035993297263738, 0.17586998530798234, 0.4414292740495746};
    private final static int ESC_TESTES=0;
    private final static double[] PESOS_TESTES = {0.3333333333333333,0.3333333333333333,0.3333333333333333};
    private final static double[] PREENCHER_PESOS_TESTES = new double [PESOS_TESTES.length];
    
    public static void output(){
        
       //Output dos resultados dos testes e do seu input relativos à classe TOPSIS
     
       System.out.println("\nTESTES ASSOCIADOS AOS MÉTODOS DA CLASSE TOPSIS");
       System.out.println("==============================================\n");
       
        System.out.println("Teste associado ao método normalizarMatrizTOPSIS"
                + "\nDados de entrada: matrizTOPSIS_testes [{7, 9, 9, 8},{8, 7, 8, 7},{9, 6, 8, 9},{6, 7, 8, 6}]"
                + "\n                  matrizTOPSISnorm_testes [{0.4615663313770509, 0.6137949055234262, 0.5447047794019222, 0.5275043787166296}, {0.5275043787166296, 0.47739603762933147, 0.48418202613504197, 0.4615663313770509},{0.5934424260562083, 0.4091966036822841, 0.48418202613504197, 0.5934424260562083}, {0.3956282840374722, 0.47739603762933147, 0.48418202613504197, 0.3956282840374722}]");
        if (teste_normalizarMatrizTOPSIS(MATRIZTOPSIS_TESTES, MATRIZTOPSISNORM_TESTES)) {
            System.out.println("Resultado esperado obtido (return=true)\n");
        } else {
            System.out.println("Resultado esperado não obtido (return = false)\n");
        }
       
       System.out.println("Teste associado ao método pesarMatrizNormalizada"
               + "\nDados de entrada: matrizTOPSISnorm_teste [{0.4615663313770509, 0.6137949055234262, 0.5447047794019222, 0.5275043787166296}, {0.5275043787166296, 0.47739603762933147, 0.48418202613504197, 0.4615663313770509},{0.5934424260562083, 0.4091966036822841, 0.48418202613504197, 0.5934424260562083}, {0.3956282840374722, 0.47739603762933147, 0.48418202613504197, 0.3956282840374722}] "
               + "\n                  vetor_pesos_teste [{0.1, 0.4, 0.3, 0.2}]"
               + "\n                  matrizNormalizadaPesada_testes [{0.04615663313770509, 0.2455179622093705, 0.16341143382057666, 0.10550087574332592}, {0.05275043787166296, 0.1909584150517326, 0.14525460784051258, 0.09231326627541019}, {0.05934424260562084, 0.16367864147291367, 0.14525460784051258, 0.11868848521124167}, {0.039562828403747224, 0.1909584150517326, 0.14525460784051258, 0.07912565680749445}]");
       if(teste_pesarMatrizNormalizada(MATRIZTOPSISNORM_TESTES,Testes.VETOR_PESOS_TESTE,MATRIZNORMALIZADAPESADA_TESTES)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return = false)\n");
       }
       
       System.out.println("Teste associado ao método solucaoIdeal"
                + "\nDados de entrada: pos_crit_teste [{1,1,1,-1}]"
                + "\n                  matrizNormalizadaPesada_testes [{0.04615663313770509, 0.2455179622093705, 0.16341143382057666, 0.10550087574332592}, {0.05275043787166296, 0.1909584150517326, 0.14525460784051258, 0.09231326627541019}, {0.05934424260562084, 0.16367864147291367, 0.14525460784051258, 0.11868848521124167}, {0.039562828403747224, 0.1909584150517326, 0.14525460784051258, 0.07912565680749445}]"
                + "\n                  vec_SI_testes [{0.05934424260562084, 0.2455179622093705, 0.16341143382057666, 0.07912565680749445}]");
        if (teste_solucaoIdeal(MATRIZNORMALIZADAPESADA_TESTES, Testes.POS_CRIT_TESTE, VEC_SI_TESTES)) {
            System.out.println("Resultado esperado obtido (return=true)\n");
        } else {
            System.out.println("Resultado esperado não obtido (return = false)\n");
        }
       
       System.out.println("Teste associado ao método solucaoIdealNegativa"
               + "\nDados de entrada: pos_crit_teste [{1,1,1,-1}]"
               + "\n                  matrizNormalizadaPesada_testes [{0.04615663313770509, 0.2455179622093705, 0.16341143382057666, 0.10550087574332592}, {0.05275043787166296, 0.1909584150517326, 0.14525460784051258, 0.09231326627541019}, {0.05934424260562084, 0.16367864147291367, 0.14525460784051258, 0.11868848521124167}, {0.039562828403747224, 0.1909584150517326, 0.14525460784051258, 0.07912565680749445}]"
               + "\n                  vec_SIN_testes [{0.039562828403747224, 0.16367864147291367, 0.14525460784051258, 0.11868848521124167}]");
       if(teste_solucaoIdealNegativa(MATRIZNORMALIZADAPESADA_TESTES, Testes.POS_CRIT_TESTE, VEC_SIN_TESTES)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return = false)\n");
       }
       
       System.out.println("Teste associado ao método findMin"
                + "\nDados de entrada: coluna_testes = 0 "
                + "\n                  matrizNormalizadaPesada_testes [{0.04615663313770509, 0.2455179622093705, 0.16341143382057666, 0.10550087574332592}, {0.05275043787166296, 0.1909584150517326, 0.14525460784051258, 0.09231326627541019}, {0.05934424260562084, 0.16367864147291367, 0.14525460784051258, 0.11868848521124167}, {0.039562828403747224, 0.1909584150517326, 0.14525460784051258, 0.07912565680749445}]"
                + "\n                  min_testes = 0.039562828403747224 ");
        if (teste_findMin(MATRIZNORMALIZADAPESADA_TESTES, COLUNA_TESTES, MIN_TESTES)) {
            System.out.println("Resultado esperado obtido (return=true)\n");
        } else {
            System.out.println("Resultado esperado não obtido (return = false)\n");
        }
       
       System.out.println("Teste associado ao método findMax"
               + "\nDados de entrada: coluna_testes = 0 "
               + "\n                  matrizNormalizadaPesada_testes [{0.04615663313770509, 0.2455179622093705, 0.16341143382057666, 0.10550087574332592}, {0.05275043787166296, 0.1909584150517326, 0.14525460784051258, 0.09231326627541019}, {0.05934424260562084, 0.16367864147291367, 0.14525460784051258, 0.11868848521124167}, {0.039562828403747224, 0.1909584150517326, 0.14525460784051258, 0.07912565680749445}]"
               + "\n                  max_testes = 0.05934424260562084 ");
       if(teste_findMax(MATRIZNORMALIZADAPESADA_TESTES, COLUNA_TESTES, MAX_TESTES)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return = false)\n");
       }
       
        System.out.println("Teste associado ao método calcularDistancias"
               + "\nDados de entrada: vec_SI_testes [{0.05934424260562084, 0.2455179622093705, 0.16341143382057666, 0.07912565680749445}] "
               + "\n                  matrizNormalizadaPesada_testes [{0.04615663313770509, 0.2455179622093705, 0.16341143382057666, 0.10550087574332592}, {0.05275043787166296, 0.1909584150517326, 0.14525460784051258, 0.09231326627541019}, {0.05934424260562084, 0.16367864147291367, 0.14525460784051258, 0.11868848521124167}, {0.039562828403747224, 0.1909584150517326, 0.14525460784051258, 0.07912565680749445}]"
               + "\n                  vec_DSI_testes [{0.029488391230979422, 0.05936165277403139, 0.0926960740246281, 0.06080887158583795}]");
       if(teste_calcularDistancias(MATRIZNORMALIZADAPESADA_TESTES, VEC_SI_TESTES, VEC_DSI_TESTES)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return = false)\n");
       }
       
        System.out.println("Teste associado ao método racioProximidadeAlternativa"
                + "\nDados de entrada: vec_DSIN_testes [{0.085116015253434, 0.040171523046841684, 0.019781414201873612, 0.04805625284826082}] "
                + "\n                  r_testes [{0.7426940888613219, 0.4035993297263738, 0.17586998530798234, 0.4414292740495746}] "
                + "\n                  vec_DSI_testes [{0.029488391230979422, 0.05936165277403139, 0.0926960740246281, 0.06080887158583795}]");
        if (teste_racioProximidadeAlternativa(VEC_DSIN_TESTES, VEC_DSI_TESTES, R_TESTES)) {
            System.out.println("Resultado esperado obtido (return=true)\n");
        } else {
            System.out.println("Resultado esperado não obtido (return = false)\n");
        }
       
       System.out.println("Teste associado ao método alternativaEscolhida"
               + "\nDados de entrada: vec_DSIN_testes [{0.085116015253434, 0.040171523046841684, 0.019781414201873612, 0.04805625284826082}] "
               + "\n                  esc_testes = 0 ");
       if(teste_alternativaEscolhida( R_TESTES, ESC_TESTES)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return = false)\n");
       }
       System.out.println("Teste associado ao método preencher_pesos"
               + "\nDados de entrada: pesos_teste [{0.3333333333333333,0.3333333333333333,0.3333333333333333}]"
               + "\n                  preencher_pesos_teste [{0,0,0}]"
               + "\n                  n_crit_teste = 3");
       if(teste_preencher_pesos(PESOS_TESTES,PREENCHER_PESOS_TESTES,Testes.N_CRIT_TESTE)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return=false\n");
       } 
    }
    
     //Métodos de Teste relativos aos métodos da classe TOPSIS
    
    /**
     * Método de teste do método TOPSIS.normalizarMatrizTOPSIS
     * @param matrizTOPSIS_teste
     * @param matrizTOPSISnorm_teste
     * @return true se o resultado esperado for obtido (todos os elementos da
     * matriz matriz são iguais aos da matrizTOPSISnorm_testes), false se
     * acontecer o oposto
     */
    private static boolean teste_normalizarMatrizTOPSIS(double[][] matrizTOPSIS_testes, double[][] matrizTOPSISnorm_testes) {

        double[][] matriz = TOPSIS.normalizarMatrizTOPSIS(matrizTOPSIS_testes);

        for (int i = 0; i < matriz.length; i++) {

            for (int k = 0; k < matriz[0].length; k++) {

                if (matriz[i][k] != matrizTOPSISnorm_testes[i][k]) {
                    return false;
                }

            }
        }

        return true;
    }
    
    /**
    * Método de teste do método TOPSIS.pesarMatrizNormalizada
    * @param arrayPesos_testes
    * @param matrizTOPSISnorm_teste
    * @param matrizNormalizadaPesada_testes
    * @return true se o resultado esperado for obtido (todos os elementos da matriz matriz são iguais aos da matrizNormalizadaPesada_testes), false se acontecer o oposto
    */
   
   private static boolean teste_pesarMatrizNormalizada(double[][] matrizTOPSISnorm_testes, double[] arrayPesos_testes, double[][] matrizNormalizadaPesada_testes){
       
       double [][] matriz= TOPSIS.pesarMatrizNormalizada(matrizTOPSISnorm_testes, arrayPesos_testes);
       
       for(int i=0;i<matriz.length;i++){
           
           for(int k=0;k<matriz[0].length;k++){
               
               if(matriz[i][k]!=matrizNormalizadaPesada_testes[i][k]){
                   return false;
               }
               
           }
       }
       
       return true;
   }
   
   /**
     * Método de teste do método TOPSIS.solucaoIdeal
     * @param vec_beneficios_testes
     * @param vec_custos_testes
     * @param matrizNormalizadaPesada_testes
     * @param vec_SI_testes
     * @return true se o resultado esperado for obtido (todos os elementos do
     * array array são iguais aos da vec_SI_testes), false se acontecer o oposto
     */

    private static boolean teste_solucaoIdeal(double[][] matrizNormalizadaPesada_testes, int[] pos_crit_teste, double[] vec_SI_testes) {

        double[] temp = TOPSIS.solucaoIdeal(matrizNormalizadaPesada_testes, pos_crit_teste);

        for (int i = 0; i < temp.length; i++) {
            if (temp[i] != vec_SI_testes[i]) {
                return false;
            }
        }

        return true;
    }
   
   /**
    * Método de teste do método TOPSIS.solucaoIdealNegativa
    * @param vec_beneficios_testes
    * @param vec_custos_testes
    * @param matrizNormalizadaPesada_testes
    * @param vec_SIN_testes
    * @return true se o resultado esperado for obtido (todos os elementos do array array são iguais aos da vec_SIN_testes), false se acontecer o oposto
    */
   
   private static boolean teste_solucaoIdealNegativa(double[][] matrizNormalizadaPesada_testes, int[] pos_crit_teste, double[] vec_SIN_testes){
       
       double[] array= TOPSIS.solucaoIdealNegativa(matrizNormalizadaPesada_testes, pos_crit_teste);
       
       for(int i=0;i<array.length;i++){
           if(array[i]!=vec_SIN_testes[i]){
               return false;
           }
       }
       return true;
   }
   
   /**
    * Método de teste do método TOPSIS.findMax
    * @param coluna_testes
    * @param matrizNormalizadaPesada_testes
    * @param max_testes
    * @return true se o resultado esperado for obtido (a variável int max igual a max_testes ), false se acontecer o oposto
    */
   
   private static boolean teste_findMax(double[][] matrizNormalizadaPesada_testes, int coluna_testes, double max_testes){
       
       double max= TOPSIS.findMax(matrizNormalizadaPesada_testes, coluna_testes);
       
       if(max!=max_testes){
           return false;
       }
       
       return true;
   }
   
   /**
     * Método de teste do método TOPSIS.findMin
     * @param coluna_testes
     * @param matrizNormalizadaPesada_testes
     * @param min_testes
     * @return true se o resultado esperado for obtido (a variável int min igual
     * a min_testes ), false se acontecer o oposto
     */
    private static boolean teste_findMin(double[][] matrizNormalizadaPesada_testes, int coluna_testes, double min_testes) {

        double min = TOPSIS.findMin(matrizNormalizadaPesada_testes, coluna_testes);

        if (min != min_testes) {
            return false;
        }

        return true;
    }

   
   /**
    * Método de teste do método TOPSIS.calcularDistancias
    * @param vec_SI_testes
    * @param matrizNormalizadaPesada_testes
    * @param vec_distancias_testes
    * @return true se o resultado esperado for obtido (todos os elementos do array array são iguais aos da vec_DSI_testes), false se acontecer o oposto
    */
   
   private static boolean teste_calcularDistancias( double[][] matrizNormalizadaPesada_testes, double[] vec_SI_testes, double[] vec_DSI_testes){
       
       double[] array= TOPSIS.calcularDistancias(matrizNormalizadaPesada_testes, vec_SI_testes);
       
       for(int i=0;i<array.length;i++){
           if(array[i]!=vec_DSI_testes[i]){
               return false;
           }
       }
       
       return true;
   }
   
    /**
     * Método de teste do método TOPSIS.racioProximidadeAternativa
     * @param vec_DSI_testes
     * @param r_testes
     * @param vec_DSIN_testes
     * @return true se o resultado esperado for obtido (todos os elementos do
     * array array são iguais aos da r_testes), false se acontecer o oposto
     */
    private static boolean teste_racioProximidadeAlternativa(double[] vec_DSIN_testes, double[] vec_DSI_testes, double[] r_testes) {

        double[] array = TOPSIS.racioProximidadeAlternativa(vec_DSIN_testes, vec_DSI_testes);

        for (int i = 0; i < array.length; i++) {

            if (array[i] != r_testes[i]) {
                return false;
            }

        }

        return true;
    }
   
   /**
    * Método de teste do método TOPSIS.alternativaEscolhida
    * @param esc_testes
    * @param r_testes
    * @return true se o resultado esperado for obtido (se a variável int esc é igual a esc_testes), false se acontecer o oposto
    */
   
   private static boolean teste_alternativaEscolhida( double[] r_testes, int esc_testes){
       
       int esc= TOPSIS.alternativaEscolhida(r_testes);
       
       if(esc!=esc_testes){
           return false;
       }
       
       return true;
   }
   
   /**
    * Método de teste do método TOPSIS.preencher_pesos
    * @param pesos_teste
    * @param preencher_pesos_teste
    * @param n_crit_teste
    * @return true se o resultado esperado for obtido, false se acontecer o oposto
    */
   
   private static boolean teste_preencher_pesos(double[] pesos_teste, double[] preencher_pesos_teste,int n_crit_teste){
       TOPSIS.preencher_pesos(preencher_pesos_teste, n_crit_teste);
       
       for(int i=0;i<pesos_teste.length;i++){
           if(pesos_teste[i]!=preencher_pesos_teste[i]){
               return false;
           }
       }
       return true;
   }
   
}
