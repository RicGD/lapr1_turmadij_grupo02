

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import lapr1.File_Input;
import lapr1.LogErros;

public class File_Input_Testes {
    
   private final static String FICH_INPUT_AHP_TESTE = "Input_AHP_TESTE.txt";
   private final static String FICH_INPUT_TOPSIS_TESTE = "Input_TOPSIS_TESTE.txt";
   private final static double [][][] CRITMATRIX_TESTE = {{{3,4,5,6},{9,0.15,2.3,5.7},{1,2,3,4},{10,20,30,40}},{{2.5,3.5,4.5,5.5},{0.4567,23,1.8,9.1},{1,2,3,5},{10.5,25,1,2}},{{1.2,1.3,1.4,11},{2.2,2,2,3},{5,4,3,2},{10,9,8,7}}};
   private final static double[][][] CRITMATRIX_TESTE_READ = new double [Testes.N_CRIT_TESTE][Testes.N_ALT_TESTE][Testes.N_ALT_TESTE];
   private final static double[][] CRITM_TESTE = {{1,2,0.3},{2,3,7},{0.9,0.8,6}};
   private final static double[][] CRITM_TESTE_READ = new double [Testes.N_CRIT_TESTE][Testes.N_CRIT_TESTE];
   private final static String[][] ALTCRIT_TOPSIS_TESTE = {{"Estilo","Confiabilidade","Consumo","Custo"},{"Civic","Saturn","Ford","Mazda"}};
   private final static String[][] ALTCRIT_TOPSIS_TESTE_READ = new String[ALTCRIT_TOPSIS_TESTE.length][ALTCRIT_TOPSIS_TESTE[0].length];
   private final static int[] POS_CRIT_TESTE_READ = new int[Testes.POS_CRIT_TESTE.length];
   private final static double[][][] ARRAY_TOPSIS_TESTE = {{{7,9,9,8},{8,7,8,7},{9,6,8,9},{6,7,8,6}},{{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}}};
   private final static double[][][] ARRAY_TOPSIS_TESTE_READ = new double[Testes.N_CRIT_TESTE][Testes.N_ALT_TESTE][Testes.N_ALT_TESTE];
   private final static String[][] CRIT_BEN_CUSTO_TESTE = new String[2][Testes.N_ALT_TESTE];
   private final static double[] VETOR_PESOS_TESTE_READ = new double[Testes.VETOR_PESOS_TESTE.length];
   
   public static void output() throws FileNotFoundException{
       
       Formatter erros_teste = new Formatter(new File(LogErros.FILE_TESTE_LOG_ERROS_LAPR1));
       
       //Output dos resultados dos testes e do seu input relativos à classe File_Input
       
       System.out.println("\nTESTES ASSOCIADOS AOS MÉTODOS DA CLASSE File_Input");
       System.out.println("================================================\n");
       System.out.println("Teste associado ao método read"
               + "\nDados de entrada: String fich_input_teste \"Input_AHP_TESTE.txt\""
               + "\n                  n_crit_teste=3"
               + "\n                  n_alt_teste=4"
               + "\n                  critMatrix_teste [{{3,4,5,6},{9,0.15,2.3,5.7},{1,2,3,4},{10,20,30,40}},{{2.5,3.5,4.5,5.5},{0.4567,23,1.8,9.1},{1,2,3,5},{10.5,25,1,2}},{{1.2,1.3,1.4,11},{2.2,2,2,3},{5,4,3,2},{10,9,8,7}]"
               + "\n                  critMatrix_teste_read [{{},{},{},{}},{{},{},{},{}},{{},{},{},{}}]"
               + "\n                  critM_teste [{1,2,0.3},{2,3,7},{0.9,0.8,6}]"
               + "\n                  critM_teste_read [{},{},{}]");
       if(teste_read(FICH_INPUT_AHP_TESTE,Testes.N_CRIT_TESTE,Testes.N_ALT_TESTE,CRITMATRIX_TESTE,CRITMATRIX_TESTE_READ,CRITM_TESTE,CRITM_TESTE_READ)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return=false)\n");
       }
       System.out.println("Teste associado ao método read_Alt_Crit_AHP"
               + "\nDados de entrada: altCrit_AHP_teste [{{\"Estilo\",\"Confiabilidade\",\"Consumo\"},{\"car1\",\"car2\",\"car3\",\"car4\"}}]"
               + "\n                  Formatter erros_teste associado ao ficheiro txt \"ErrosTESTE_LAPR1\""
               + "\n                  String fich_input_AHP_teste \"Input_AHP_TESTE.txt\"");
       if(teste_read_Alt_Crit_AHP(Testes.ALTCRIT_AHP_TESTE,FICH_INPUT_AHP_TESTE,erros_teste)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return=false)\n");
       }
       System.out.println("Teste associado ao método read_Alt_Crit_TOPSIS"
               + "\nDados de entrada: altCrit_TOPSIS_teste [{{\"Estilo\",\"Confiabilidade\",\"Consumo\",\"Custo\"},{\"Civic\",\"Saturn\",\"Ford\",\"Mazda\"}}]"
               + "\n                  altCrit_TOPSIS_teste_read"
               + "\n                  String fich_input_TOPSIS_teste \"Input_TOPSIS_TESTE.txt\""
               + "\n                  Formatter erros_teste associado ao ficheiro txt \"ErrosTESTE_LAPR1\"");
       if(teste_read_Alt_Crit_TOPSIS(ALTCRIT_TOPSIS_TESTE,ALTCRIT_TOPSIS_TESTE_READ,FICH_INPUT_TOPSIS_TESTE,erros_teste)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return=false)\n");
       }
       System.out.println("Teste associado ao método preenchernum_TOPSIS"
               + "\nDados de entrada: String fich_input_TOPSIS_teste \"Input_TOPSIS_TESTE.txt\""
               + "\n                  pos_crit_teste [{1,1,1,-1}]"
               + "\n                  pos_crit_teste_read [{}]"
               + "\n                  altCrit_TOPSIS_teste [{{\"Estilo\",\"Confiabilidade\",\"Consumo\",\"Custo\"},{\"Civic\",\"Saturn\",\"Ford\",\"Mazda\"}}]"
               + "\n                  crit_ben_custo_teste [{},{}]"
               + "\n                  array_TOPSIS_teste {{{7,9,9,8},{8,7,8,7},{9,6,8,9},{6,7,8,6}},{{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}}}"
               + "\n                  array_TOPSIS_teste_read [{{},{},{},{}},{{},{},{},{}},{{},{},{},{}}]"
               + "\n                  vetor_pesos_teste [{0.1, 0.4, 0.3, 0.2}]"
               + "\n                  vetor_pesos_read [{}]");
       if(teste_preenchernum_TOPSIS(FICH_INPUT_TOPSIS_TESTE,Testes.POS_CRIT_TESTE,POS_CRIT_TESTE_READ,ALTCRIT_TOPSIS_TESTE,CRIT_BEN_CUSTO_TESTE,ARRAY_TOPSIS_TESTE,ARRAY_TOPSIS_TESTE_READ,Testes.VETOR_PESOS_TESTE,VETOR_PESOS_TESTE_READ)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return=false)\n");
       }  
    
   } 
    public static boolean teste_read(String fich_input_teste,int n_crit_teste,int n_alt_teste,double[][][] critMatrix_teste,double[][][] critMatrix_teste_read,double[][] critM_teste,double[][] critM_teste_read) throws FileNotFoundException{
        
        File_Input.read(fich_input_teste, n_crit_teste, n_alt_teste, critMatrix_teste_read, critM_teste_read);
        
        for(int i=0;i<critM_teste.length;i++){
            for(int j=0;j<critM_teste[i].length;j++){
                if(critM_teste[i][j]!=critM_teste_read[i][j]){
                    return false;
                }
            }
        }
        
        for(int i=0;i<critMatrix_teste.length;i++){
            for(int j=0;j<critMatrix_teste[i].length;j++){
                for(int k=0;k<critMatrix_teste[i][j].length;k++){
                    if(critMatrix_teste[i][j][k]!=critMatrix_teste_read[i][j][k]){
                        return false;
                    }
                }
            }
        }
        
        return true;
    }
    
    public static boolean teste_read_Alt_Crit_AHP(String[][] altCrit_AHP_teste,String fich_input_teste,Formatter erros_teste) throws FileNotFoundException{
        
        String[][] temp = File_Input.read_Alt_Crit_AHP(fich_input_teste, erros_teste);
        
        for(int i=0;i<altCrit_AHP_teste.length;i++){
            for(int j=0;j<altCrit_AHP_teste[i].length;j++){
                if(!altCrit_AHP_teste[i][j].equals(temp[i][j])){
                    return false;
                }
            }
        }
        return true;
    }
    
    public static boolean teste_read_Alt_Crit_TOPSIS(String[][] altCrit_teste_TOPSIS,String[][] altCrit_teste_TOPSIS_read, String fich_input_TOPSIS_teste,Formatter erros_teste) throws FileNotFoundException{
        
        File_Input.read_Alt_Crit_TOPSIS(altCrit_teste_TOPSIS_read, fich_input_TOPSIS_teste, erros_teste);
        
        for(int i=0;i<altCrit_teste_TOPSIS.length;i++){
            for(int j=0;j<altCrit_teste_TOPSIS[i].length;j++){
                if(!altCrit_teste_TOPSIS[i][j].equals(altCrit_teste_TOPSIS_read[i][j])){
                    return false;
                }
            }
        }
        
        return true;
    }
    
    public static boolean teste_preenchernum_TOPSIS(String fich_input_TOPSIS_teste, int[] pos_crit_teste, int[] pos_crit_teste_read,String[][] altCrit_TOPSIS_teste,String[][] crit_ben_custo_teste,double[][][] array_TOPSIS_teste,double[][][] array_TOPSIS_teste_read,double[] vetor_pesos_teste,double[] vetor_pesos_teste_read) throws FileNotFoundException{
        
        File_Input.preenchernum_TOPSIS(pos_crit_teste_read, altCrit_TOPSIS_teste, crit_ben_custo_teste, fich_input_TOPSIS_teste, array_TOPSIS_teste_read, vetor_pesos_teste_read);
        
        for(int i=0;i<pos_crit_teste.length;i++){
            if(pos_crit_teste[i]!=pos_crit_teste_read[i]){
                return false;
            }
        }
        
        for(int i=0;i<vetor_pesos_teste.length;i++){
            if(vetor_pesos_teste[i]!=vetor_pesos_teste_read[i]){
                return false;
            }
        }
        
        for(int i=0;i<array_TOPSIS_teste.length;i++){
            for(int j=0;j<array_TOPSIS_teste[i].length;j++){
                for(int k=0;k<array_TOPSIS_teste[i][j].length;k++){
                    if(array_TOPSIS_teste[i][j][k]!=array_TOPSIS_teste_read[i][j][k]){
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
