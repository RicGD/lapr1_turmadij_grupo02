
import lapr1.AHP;
import org.la4j.Matrix;


public class AHP_Testes {
    
       private final static double[][] MATRIZ_NORM_TESTE ={{0.3, 0.2857142857142857, 0.375},{0.6, 0.5714285714285714, 0.5},{0.09999999999999999, 0.14285714285714285, 0.125}};
       private final static double[] EIGENVECTOR_EX_TESTE ={0.3196182639359758, 0.558424543094797, 0.12195719296922707};
       private final static double[] EIGENVECTOR_APROX_TESTE = {0.3202380952380952, 0.557142857142857, 0.12261904761904761};
       private final static double[][] MATRIZ_PRIORIDADE_TESTE = {{0.1806980529373981, 0.7320266660907263, 0.1806980529373981}, {0.3849582479339818, 0.38837766526584344, 0.3849582479339818}, {0.09359151053839163, 0.14669905691949398, 0.09359151053839163}, {0.9002199676564477, 0.540165841157768, 0.9002199676564477}};
       private final static double[][] VECS_TESTE = {{0.1806980529373981, 0.3849582479339818, 0.09359151053839163, 0.9002199676564477},{0.7320266660907263, 0.38837766526584344, 0.14669905691949398, 0.540165841157768},{0.1806980529373981, 0.3849582479339818, 0.09359151053839163, 0.9002199676564477}};
       private final static double[] PRIORIDADECOMPOSTA_TESTE = {0.48786685169425226, 0.38686335187601895, 0.12318000066500576, 0.6996183828928976};
       private final static double MAX_EIGENVALUE_EX_TESTE = 3.0182947072896313;
       private final static double MAX_EIGENVALUE_APROX_TESTE = 3.01832479413957;
       private final static double IC_TESTE = 0.00914735364481567;
       private final static double RC_TESTE = 0.015771299387613225;
       private final static String SOLUCAO_TESTE = "A alternativa 4 é a mais viável";
       private final static double[] RCV_TESTE={0.45,0.6,0.2,0.4};
       private final static double[] CRITVECTOR_TESTE={0.45,0.3,0.67,0.84};
       private final static double LIMIAR_ATRIB_TESTE = 0.33;
       private final static double LIMIAR_RC_TESTE = 0.7;
       private final static double[] CRITVECTOR2_TESTE={0.55,0,0.77,0.94};
       private final static double[] VEC_TESTE = {3,4,5};
       private final static double[] VEC_NORM_TESTE = {0.25,0.3333333333333333,0.4166666666666667};
       private final static int N_CRIT_LIMIAR_RC_TESTE = 4;
    
    public static void output(){   
     //Output dos resultados dos testes e do seu input relativos à classe AHP
       
       System.out.println("\nTESTES ASSOCIADOS AOS MÉTODOS DA CLASSE AHP");
       System.out.println("==============================================\n");
       System.out.println("Teste associado ao método normalizarMatriz"
               + "\nDados de entrada: matriz_teste [{1.0, 0.5, 3.0},{2.0, 1.0, 4.0},{0.3333333333333333, 0.25, 1.0}]"
               + "\n                  matriznorm_teste [{0.3, 0.2857142857142857, 0.375},{0.6, 0.5714285714285714, 0.5},{0.09999999999999999, 0.14285714285714285, 0.125}]");
       if(teste_normalizarMatrizAHP(Testes.MATRIZ_TESTE,MATRIZ_NORM_TESTE)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return = false)\n");
       }
       System.out.println("Teste associado ao método EigenVectorExato"
               + "\nDados de entrada: matriz_teste [{1.0, 0.5, 3.0},{2.0, 1.0, 4.0},{0.3333333333333333, 0.25, 1.0}]"
               + "\n                  eigenvector_ex_teste [{0.4880564875326826, 0.8527132263927324, 0.186228404149738}]");
       if(teste_EigenVectorExato(Testes.MATRIZ_TESTE,EIGENVECTOR_EX_TESTE)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return = false)\n");    
       }
       System.out.println("Teste associado ao método EigenVectorAprox"
               + "\nDados de entrada: matriznorm_teste [{0.3, 0.2857142857142857, 0.375},{0.6, 0.5714285714285714, 0.5},{0.09999999999999999, 0.14285714285714285, 0.125}]"
               + "\n                  eigenvector_aprox_teste [{0.3202380952380952, 0.557142857142857, 0.12261904761904761}]");
       if(teste_EigenVectorAprox(MATRIZ_NORM_TESTE,EIGENVECTOR_APROX_TESTE)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return = false)\n");    
       }
       System.out.println("Teste associado ao método Array_Matrix"
               + "\nDados de entrada: matriz_teste matriz_teste [{1.0, 0.5, 3.0},{2.0, 1.0, 4.0},{0.3333333333333333, 0.25, 1.0}]");
       if(teste_array_Matrix(Testes.MATRIZ_TESTE)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return=false)\n");
       }
       System.out.println("Teste associado ao método maxEigenValueExato"
               + "\nDados de entrada: matriz_teste [{1.0, 0.5, 3.0},{2.0, 1.0, 4.0},{0.3333333333333333, 0.25, 1.0}]"
               + "\n                  max_eigenvalue_teste_ex=3.0182947072896313");
       if(teste_maxEigenValueExato(Testes.MATRIZ_TESTE,MAX_EIGENVALUE_EX_TESTE)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return=false)\n");
       }
       System.out.println("Teste associado ao método maxEigenValueAprox"
               + "\nDados de entrada: matriz_teste [{1.0, 0.5, 3.0},{2.0, 1.0, 4.0},{0.3333333333333333, 0.25, 1.0}]"
               + "\n                  max_eigenvalue_teste_aprox=3.01832479413957");
       if(teste_maxEigenValueAprox(Testes.MATRIZ_TESTE,MAX_EIGENVALUE_APROX_TESTE,EIGENVECTOR_APROX_TESTE)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return=false)\n");
       }
       System.out.println("Teste associado ao método calcularIC"
               + "\nDados de entrada: IC_teste=0.00914735364481567"
               + "\n                  max_eigenvalue_teste_ex=3.01832479413957"
               + "\n                  matriz_teste [{1.0, 0.5, 3.0},{2.0, 1.0, 4.0},{0.3333333333333333, 0.25, 1.0}]");
       if(teste_calcularIC(IC_TESTE,MAX_EIGENVALUE_EX_TESTE,Testes.MATRIZ_TESTE)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return=false)\n");
       }
       System.out.println("Teste associado ao método calcularRC"
               + "\nDados de entrada: RC_teste=0.015771299387613225"
               + "\n                  IC_teste=0.00914735364481567");
       if(teste_calcularRC(RC_TESTE,IC_TESTE,Testes.MATRIZ_TESTE,Testes.N_CRIT_TESTE,Testes.N_ALT_TESTE)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return=false)\n");
       }
       System.out.println("Teste associado ao método MatrizPrioridade"
               + "\nDados de entrada: MatrizPrioridade_teste [{0.1806980529373981, 0.7320266660907263, 0.1806980529373981}, {0.3849582479339818, 0.38837766526584344, 0.3849582479339818}, {0.09359151053839163, 0.14669905691949398, 0.09359151053839163}, {0.9002199676564477, 0.540165841157768, 0.9002199676564477}] "
               + "\n                  vec1_teste [0.1806980529373981, 0.3849582479339818, 0.09359151053839163, 0.9002199676564477]"
               + "\n                  vec2_teste [0.7320266660907263, 0.38837766526584344, 0.14669905691949398, 0.540165841157768]"
               + "\n                  vec3_teste [0.1806980529373981, 0.3849582479339818, 0.09359151053839163, 0.9002199676564477]");
       if(teste_matrizPrioridade(MATRIZ_PRIORIDADE_TESTE,VECS_TESTE)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return=false)\n");
       }
       System.out.println("Teste associado ao método VetorPrioridadeComp"
               + "\nDados de entrada: PrioridadeComposta_teste [0.2876375270372197, 0.22410191270455726, 0.07540437350388046, 0.41285618675434266]"
               + "\n                  MatrizPrioridade_teste [{0.13078762589071868, 0.4123130894870025, 0.13078762589071868}, {0.24443179958643876, 0.20794225902921554, 0.24443179958643876}, {0.06570416982788117, 0.08311479181044398, 0.06570416982788117}, {0.5590764046949614, 0.29662985967333794, 0.5590764046949614}]"
               + "\n                  eigenvector_aprox_teste [0.32023809523809527,0.5571428571428572,0.12261904761904761]");
       if(teste_vetorPrioridadeComp(PRIORIDADECOMPOSTA_TESTE,MATRIZ_PRIORIDADE_TESTE,EIGENVECTOR_APROX_TESTE)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return=false)\n");
       }
       System.out.println("Teste associado ao método findMax"
               + "\nDados de entrada: PrioridadeComposta_teste [0.2876375270372197, 0.22410191270455726, 0.07540437350388046, 0.41285618675434266]"
               + "\n                  solucao_teste \"A alternativa 4 é a mais viável\"");
       if(teste_findMax(PRIORIDADECOMPOSTA_TESTE,SOLUCAO_TESTE)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultao esperado não obtido (return=false)\n");
       }
       System.out.println("Teste associado ao método regraSaaty"
               + "\nDados de entrada: matriz_teste [{1.0, 0.5, 3.0},{2.0, 1.0, 4.0},{0.3333333333333333, 0.25, 1.0}]");
       if(teste_regraSaaty(Testes.MATRIZ_TESTE)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return=false)\n");
       }
       
       System.out.println("Teste associado ao método limiarCritRC"
               + "\nDados de entrada: RC_teste = 0.015771299387613225"
               + "\n                  RCv_teste=[{0.45,0.6,0.2,0.4}]"
               + "\n                  critVector_teste=[{0.45,0.3,0.67,0.84}]"
               + "\n                  limiar_atrib_teste = 0.33"
               + "\n                  limiar_RC_teste = 0.7"
               + "\n                  n_crit_teste = 4"
               + "\n                  critVector2_teste=[{0.55,0,0.77,0.94}]"
               + "\n                  crit_Elim_Testes=[{\"\",\"\",\"\"}]"
               + "\n                  altCrit_AHP_Testes=[{{\"Estilo\",\"Confiabilidade\",\"Consumo\"},{\"car1\",\"car2\",\"car3\",\"car4\"}}]");
       if(teste_limiarCritRC(RC_TESTE, RCV_TESTE, CRITVECTOR_TESTE, LIMIAR_ATRIB_TESTE, LIMIAR_RC_TESTE, N_CRIT_LIMIAR_RC_TESTE, CRITVECTOR2_TESTE, Testes.CRIT_ELIM_TESTE,Testes.ALTCRIT_AHP_TESTE)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return = false)\n");
       }
       System.out.println("Teste associado ao método normalizar_Vetor"
               + "\nDados de entrada: vec_teste [{3,4,5}]"
               + "\n                  vec_norm_teste [{0.25,0.3333333333333333,0.4166666666666667}]");
       if(teste_normalizar_Vetor(VEC_NORM_TESTE,VEC_TESTE)){
           System.out.println("Resultado esperado obtido (return=true)\n");
       }else{
           System.out.println("Resultado esperado não obtido (return=false)\n");
       }
    }
    
    /**
    * Método de teste do método AHP.NormalizarMatriz
    * @param matriz_teste
    * @param matriznorm_teste
    * @return true se o resultado esperado for obtido (todos os elementos da matriz temp são iguais aos da matriznorm_teste), false se acontecer o oposto
    */
   
   public static boolean teste_normalizarMatrizAHP(double[][] matriz_teste,double[][] matriznorm_teste){
       double[][] temp = new double[matriz_teste.length][matriz_teste.length];
       for(int i=0;i<temp.length;i++){
           for(int j=0;j<temp[i].length;j++){
               temp[i][j]=matriz_teste[i][j];
           }
       }
       AHP.normalizarMatriz(temp);
       for(int i=0;i<temp.length;i++){
           for(int j=0;j<temp[i].length;j++){
               if(temp[i][j]!=matriznorm_teste[i][j]){
                   return false;
               }
           }
       }
       return true;
   }
   
   /**
    * Método de teste do método AHP.EigenVectorExato
    * @param matriz_teste
    * @param eigenvector_ex_teste
    * @return true se o resultado esperado for obtido (todos os elementos do vetor_proprio são iguais aos do eigenvector_ex_teste), false se acontecer o oposto
    */
   
   public static boolean teste_EigenVectorExato(double[][] matriz_teste,double[] eigenvector_ex_teste){
       double[] vetor_proprio = AHP.EigenVectorExato(matriz_teste);

       for(int i=0;i<vetor_proprio.length;i++){
           if(vetor_proprio[i]!=eigenvector_ex_teste[i]){
               return false;
           }
       }
       return true;
   }
   
   /**
    * Método de teste do método AHP.EigenVectorAprox
    * @param matriznorm_teste
    * @param eigenvector_aprox_teste
    * @return true se o resultado esperado for obtido (todos os elementos do vetor_proprio são iguais aos do eigenvector_aprox_teste), false se acontecer o oposto
    */
   
   public static boolean teste_EigenVectorAprox(double[][] matriznorm_teste,double[] eigenvector_aprox_teste){
       double[] vetor_proprio = AHP.EigenVectorAprox(matriznorm_teste);
       for(int i=0;i<vetor_proprio.length;i++){
           if(vetor_proprio[i]!=eigenvector_aprox_teste[i]){
               return false;
           }
       }
       return true;
   }
   
   /**
    * Método de teste do método AHP.Array_Matrix
    * @param matriz_teste
    * @return true se o resultado esperado for obtido (todos os elementos do array são iguais aos da matriz_teste), false se acontecer o oposto
    */
   
   public static boolean teste_array_Matrix(double[][] matriz_teste){
       //Melhorar método se possivel
        Matrix temp = AHP.array_Matrix(matriz_teste);
        double[][] array = temp.toDenseMatrix().toArray();
        for(int i=0;i<array.length;i++){
            for(int j=0;j<array[i].length;j++){
                if(array[i][j]!=matriz_teste[i][j]){
                    return false;
                }
            }
        }
       return true;
   }
   
   /**
    * Método de teste do método AHP.maxEigenValueExato
    * @param matriz_teste
    * @param max_eigenvalue_teste_ex
    * @return true se o resultado esperado for obtido (o valor de max_eigenvalue_temp=max_eigenvalue_teste_ex, false se acontecer o oposto
    */
   
   public static boolean teste_maxEigenValueExato(double[][] matriz_teste,double max_eigenvalue_teste_ex){
       double max_eigenvalue_temp = AHP.maxEigenValueExato(matriz_teste);
       return max_eigenvalue_temp==max_eigenvalue_teste_ex;
   }
   
   /**
    * Método de teste do método AHP.maxEigenValueAprox
    * @param matriz_teste
    * @param max_eigenvalue_teste_aprox
    * @param eigenvector_aprox_teste
    * @return true se o resultado esperado for obtido(o valor de max_eigenvalue_temp=max_eigenvalue_teste_aprox), false se acontecer o oposto 
    */
   
   public static boolean teste_maxEigenValueAprox(double[][] matriz_teste,double max_eigenvalue_teste_aprox,double[] eigenvector_aprox_teste){
       double max_eigenvalue_temp = AHP.maxEigenValueAprox(matriz_teste,eigenvector_aprox_teste);
       return max_eigenvalue_temp==max_eigenvalue_teste_aprox;
   }
   
   /**
    * Método de teste do método AHP.calcularIC
    * @param IC_teste
    * @param max_eigenvalue_teste
    * @param matriz_teste
    * @return true se o resultado esperado for obtido (o valor de IC_temp=IC_teste), false se acontecer o oposto
    */
   
   public static boolean teste_calcularIC(double IC_teste,double max_eigenvalue_teste,double[][] matriz_teste){
       double IC_temp = AHP.calcularIC(max_eigenvalue_teste, matriz_teste);
       return IC_temp==IC_teste;
   }
   
   /**
    * Método de teste do método AHP.calcularRC
    * @param RC_teste
    * @param IC_teste
    * @param matriz_teste
    * @return true se o resultado esperado for obtido (o valor de RC_temp=RC_teste), false se acontecer o oposto 
    */
   
   public static boolean teste_calcularRC(double RC_teste,double IC_teste,double[][] matriz_teste,int n_crit,int n_alt){
       double RC_temp =AHP.calcularRC(IC_teste, matriz_teste, n_crit, n_alt);
       return RC_temp==RC_teste;
       
   }
   
   /**
    * Método de teste do método AHP.MatrizPrioridade
    * @param MatrizPrioridade_teste
    * @param vecs_teste
    * @return true se o resultado esperado for obtido (todos os elementos da matriz MatrizPrioridade_temp serem iguais aos da MatrizPrioridade_teste), false se acontecer o oposto
    */
   
   public static boolean teste_matrizPrioridade(double[][] MatrizPrioridade_teste,double[][]vecs_teste){
       double[][] MatrizPrioridade_temp = AHP.matrizPrioridade(vecs_teste);
       for(int i=0;i<MatrizPrioridade_temp.length;i++){
           for(int j=0;j<MatrizPrioridade_temp[i].length;j++){
               if(MatrizPrioridade_temp[i][j]!=MatrizPrioridade_teste[i][j]){
                   return false;
               }
           }
       }
       return true;
   }
   
   /**
    * Método de teste associado ao método AHP.VetorPrioridadeComp
    * @param PrioridadeComposta_teste
    * @param MatrizPrioridade_teste
    * @param eigenvector_aprox_teste
    * @return true se o resultado esperado for obtido (os elementos do vetor PrioridadeComposta_temp serem iguais aos do PrioridadeComposta_teste), false se acontecer o oposto
    */
   
   public static boolean teste_vetorPrioridadeComp(double[] PrioridadeComposta_teste,double[][] MatrizPrioridade_teste,double[] eigenvector_aprox_teste){
       double[] PrioridadeComposta_temp = AHP.vetorPrioridadeComp(MatrizPrioridade_teste, eigenvector_aprox_teste);

       for(int i=0;i<PrioridadeComposta_temp.length;i++){
           if(PrioridadeComposta_temp[i]!=PrioridadeComposta_teste[i]){
               return false;
           }
       }
       return true;
   }
   
   /**
    * Método de teste do método AHP.findMax
    * @param PrioridadeComposta_teste
    * @param solucao_teste
    * @return true se o resultado esperado for obtido (se a string solucao_temp=solucao_teste), false se acontecer o oposto
    */
   
   public static boolean teste_findMax(double[] PrioridadeComposta_teste, String solucao_teste){
       String solucao_temp = AHP.findMax(PrioridadeComposta_teste);
       return solucao_temp.equals(solucao_teste);
   }
   
   /**
    * Método de teste do método AHP.regraSaaty
    * @param matriz_teste, matriz dada 
    * @return true se o método estiver a funcionar, false se acontecer oposto 
    */
   public static boolean teste_regraSaaty(double[][] matriz_teste){
       return AHP.regraSaaty(matriz_teste);
   }
      
    /**
    * Método de teste do método AHP.limiarCritRC
    * @param RC_teste
    * @param RCv_teste
    * @param critVector_teste
    * @param limiar_atrib_teste
    * @param limiar_RC_teste
    * @param n_crit_teste
    * @param critVector2_teste
    * @param critElim_teste
    * @param altCrit_AHP_teste
    * @return true se o método estiver a funcionar, false se acontecer oposto 
    */
   
   public static boolean teste_limiarCritRC(double RC_teste,double[] RCv_teste,double[] critVector_teste,double limiar_atrib_teste,double limiar_RC_teste,int n_crit_teste,double[] critVector2_teste,String [] critElim_teste,String [][] altCrit_AHP_teste){
       
       AHP.limiarCritRC(RC_teste,RCv_teste,critVector_teste,limiar_atrib_teste,limiar_RC_teste,n_crit_teste,critElim_teste,altCrit_AHP_teste);
       
       if(!critElim_teste[0].equals(altCrit_AHP_teste[0][1])){
           return false;
       }
       
       for(int i=0;i<critVector_teste.length;i++){
           if(critVector_teste[i]!=critVector2_teste[i]){
               return false;
           }
       }
       return true;
   }
   
   public static boolean teste_normalizar_Vetor(double[] vec_norm_teste,double[] vec_teste){
       
       double[] temp = AHP.normalizar_Vetor(vec_teste);
       
       for(int i=0;i<vec_norm_teste.length;i++){
           if(temp[i]!=vec_norm_teste[i]){
               return false;
           }
       }
       return true;
   }
    
}
