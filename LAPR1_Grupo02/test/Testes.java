
import java.io.FileNotFoundException;

//Classe de testes unitários de todos os métodos das classes AHP, Matrizes_Vetores, TOPSIS e File_Input

public class Testes {
    
    public final static double[][] MATRIZ_TESTE = {{1.0, 0.5, 3.0},{2.0, 1.0, 4.0},{0.3333333333333333, 0.25, 1.0}};
    public final static int N_CRIT_TESTE = 3;
    public final static int N_ALT_TESTE = 4;
    public final static int[] POS_CRIT_TESTE = {1,1,1,-1};
    public static final double[] VETOR_PESOS_TESTE = {0.1,0.4,0.3,0.2};
    public final static String[] CRIT_ELIM_TESTE = {"","",""};
    public final static String[][] ALTCRIT_AHP_TESTE = {{"Estilo","Confiabilidade","Consumo"},{"car1","car2","car3","car4"}};
    
   public static void main(String[] args) throws FileNotFoundException{ 
       
       File_Input_Testes.output();
       Matrizes_Vetores_Testes.output();
       AHP_Testes.output();
       TOPSIS_Testes.output();
 
   }   
}
